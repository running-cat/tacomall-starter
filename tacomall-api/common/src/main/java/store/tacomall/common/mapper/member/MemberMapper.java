package store.tacomall.common.mapper.member;

import org.springframework.stereotype.Repository;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;

import store.tacomall.common.entity.member.Member;

@Repository
public interface MemberMapper extends BaseMapper<Member> {

}