/***
 * @Author: 码上talk|RC
 * @Date: 2021-08-02 15:09:33
 * @LastEditTime: 2021-10-11 22:10:14
 * @LastEditors: 码上talk|RC
 * @Description: 
 * @FilePath: /tacomall-api/common/src/main/java/store/tacomall/common/constant/RedisKeyConstant.java
 * @微信:  13680065830
 * @邮箱:  3189482282@qq.com
 * @oops: Just do what I think it is right
 */
package store.tacomall.common.constant;

public class RedisKeyConstant {
    public static final String COMMON_MOBILE_VERIFY_CODE = "common:mobile:verify_code-";
    public static final String COMMON_MEMBER_REGISTER_LAST_INVITE_CODE = "common:member:register_last_invite_code";
    public static final String COMMON_NO_REPEAT_SUBMIT = "common:no_repeat_submit-";
}
