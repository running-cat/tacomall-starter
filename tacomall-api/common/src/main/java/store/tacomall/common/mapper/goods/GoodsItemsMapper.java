package store.tacomall.common.mapper.goods;

import store.tacomall.common.entity.goods.GoodsItems;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import org.apache.ibatis.annotations.Mapper;

/**
 * <p>
 *  Mapper 接口
 * </p>
 *
 * @author 码上talk
 * @since 2021-10-12
 */
@Mapper
public interface GoodsItemsMapper extends BaseMapper<GoodsItems> {

}
