package store.tacomall.common.util;

public class MathUtil {
  public static int getGcd(int a, int b) {
    int max, min;
    max = (a > b) ? a : b;
    min = (a < b) ? a : b;

    if (max % min != 0) {
      return getGcd(min, max % min);
    } else
      return min;
  }

  public static int getLcm(int n1, int n2) {
    return n1 * n2 / getGcd(n1, n2);
  }

  public static Integer getLcmInList(Integer[] ints) {
    int min = 0;
    if (ints.length == 1) {
      min = ints[0];
    } else {
      for (int i = 0; i < ints.length; i++) {
        if (i < ints.length - 1) {
          min = getLcm(ints[i], ints[i + 1]);
        }
      }
    }
    return min;
  }
}
