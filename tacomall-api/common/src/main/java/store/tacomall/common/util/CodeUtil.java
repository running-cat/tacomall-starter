/***
 * @Author: 码上talk|RC
 * @Date: 2021-09-18 14:01:11
 * @LastEditTime: 2021-10-06 14:56:45
 * @LastEditors: 码上talk|RC
 * @Description: 
 * @FilePath: /tacomall-api/common/src/main/java/store/tacomall/common/util/CodeUtil.java
 * @微信:  13680065830
 * @邮箱:  3189482282@qq.com
 * @oops: Just do what I think it is right
 */
/***
 * @Author: 码上talk|RC
 * @Date: 2021-04-13 16:47:23
 * @LastEditTime: 2021-09-04 22:18:33
 * @LastEditors: 码上talk|RC
 * @Description: 
 * @FilePath: /tacomall-api/common/src/main/java/cn/tacomall/common/util/CodeUtil.java
 * @微信:  13680065830
 * @邮箱:  3189482282@qq.com
 * @oops: Just do what I think it is right
 */
package store.tacomall.common.util;

import java.text.DecimalFormat;

import store.tacomall.common.constant.RedisKeyConstant;

public class CodeUtil {

  private static final String MEMBER_INVITE_CODE_STR_FORMAT = "00000000";

  private RedisUtil redisUtil;

  public CodeUtil(RedisUtil rdu) {
    redisUtil = rdu;
  }

  public String genMemberInviteCode() {
    DecimalFormat df = new DecimalFormat(MEMBER_INVITE_CODE_STR_FORMAT);
    return df.format(redisUtil.incrBy(RedisKeyConstant.COMMON_MEMBER_REGISTER_LAST_INVITE_CODE, 1, 0));
  }

}
