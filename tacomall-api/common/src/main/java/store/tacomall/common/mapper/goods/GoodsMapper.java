package store.tacomall.common.mapper.goods;

import org.springframework.stereotype.Repository;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;

import store.tacomall.common.entity.goods.Goods;

@Repository
public interface GoodsMapper extends BaseMapper<Goods> {

}
