/***
 * @Author: 码上talk|RC
 * @Date: 2021-04-13 16:47:23
 * @LastEditTime: 2021-10-06 14:56:39
 * @LastEditors: 码上talk|RC
 * @Description: 
 * @FilePath: /tacomall-api/common/src/main/java/store/tacomall/common/util/SnUtil.java
 * @微信:  13680065830
 * @邮箱:  3189482282@qq.com
 * @oops: Just do what I think it is right
 */
package store.tacomall.common.util;

import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.util.Random;

public class SnUtil {

    private static final int TYPE_GOODS = 1;
    private static final int TYPE_GOODS_ITEMS = 2;
    private static final int TYPE_APPROVAL = 3;
    private static final int TYPE_PURCHANSE_STANDARD = 4;
    private static final int TYPE_PURCHANSE_PRESTORE = 5;
    private static final int TYPE_PURCHANSE_PRESALE = 6;
    private static final int TYPE_PURCHANSE_NEW_DISTRIBUTOR = 7;
    private static final int TYPE_PURCHANSE_CUSTOMIZE = 8;
    private static final int TYPE_MEMBER = 9;
    private static final int TYPE_SALE = 10;
    private static final int TYPE_STOCK_IN = 11;
    private static final int TYPE_STOCK_CHECK = 12;

    private static final int TYPE_ERP_STOCK_IN = 13;
    private static final int TYPE_ERP_STOCK_OUT = 14;
    private static final int TYPE_ERP_STOCK_CHECK = 15;

    private static final int TYPE_TOP_UP = 16;
    private static final int TYPE_FORM_EXCHANGE = 17;
    private static final int TYPE_SALE_RETURN = 18;

    private static final int TYPE_SHOP = 19;
    private static final int TYPE_BRIEFS_FORM = 20;

    private static final int TYPE_REVERT_FORM = 21;
    private static final int TYPE_SHAREHOLDER_CONTRACT = 22;
    private static final int TYPE_MEMBER_TRANSFER = 23;
    private static final int TYPE_Tm_SELF_EMPLOYED_LABORER = 24;

    private static final int TYPE_LOGISTICS_DEPPON = 25;
    private static final int TYPE_LOGISTICS_OTHERS = 26;

    private static final int TYPE_PAY_FORM = 27;

    private static final int TYPE_PURCHANSE_SIXUE = 28;
    private static final int TYPE_PURCHANSE_SIXUE_PRESALE = 29;
    private static final int TYPE_Tm_PAY_FORM = 30;

    private static final int TYPE_SHAREHOLDER_CONTRACT_CANCEL = 31;

    private static final String CODE_GOODS = "SPBS";
    private static final String CODE_GOODS_ITEMS = "SPGI";
    private static final String CODE_APPROVAL = "SHTK";
    private static final String CODE_PURCHANSE_STANDARD = "CGBZ";
    private static final String CODE_PURCHANSE_PRESTORE = "CGZH";
    private static final String CODE_PURCHANSE_PRESALE = "CGYS";
    private static final String CODE_PURCHANSE_NEW_DISTRIBUTOR = "CGXD";
    private static final String CODE_PURCHANSE_CUSTOMIZE = "CGZD";
    private static final String CODE_MEMBER = "HYWQ";
    private static final String CODE_SALE = "XSDD";
    private static final String CODE_STOCK_IN = "RKYM";
    private static final String CODE_STOCK_CHECK = "PDJH";

    private static final String CODE_ERP_STOCK_IN = "RKMP";
    private static final String CODE_ERP_STOCK_OUT = "CKLY";
    private static final String CODE_ERP_STOCK_CHECK = "PDSF";

    private static final String CODE_TOP_UP = "CZCQ";
    private static final String CODE_FORM_EXCHANGE = "THLO";
    private static final String CODE_SALE_RETURN = "TDTH";

    private static final String CODE_SHOP = "DPSD";
    private static final String CODE_BRIEFS_FORM = "HDNK";

    private static final String CODE_REVERT_FORM = "THHD";
    private static final String CODE_SHAREHOLDER_CONTRACT = "SCGD";
    private static final String CODE_MEMBER_TRANSFER = "ZYHY";
    private static final String CODE_Tm_SELF_EMPLOYED_LABORER = "GTHS";

    private static final String CODE_LOGISTICS_DEPPON = "QUFH";
    private static final String CODE_LOGISTICS_OTHERS = "REWX";

    private static final String CODE_PAY_FORM = "ZFKW";

    private static final String CODE_PURCHANSE_SIXUE = "SXCG";
    private static final String CODE_PURCHANSE_SIXUE_PRESALE = "SXYS";

    private static final String CODE_Tm_PAY_FORM = "TmP";

    private static final String CODE_SHAREHOLDER_CONTRACT_CANCEL = "QXGD";

    private String dateStr() {
        DateTimeFormatter fmt = DateTimeFormatter.ofPattern("yyyyMMddHHmmssSSS");
        return LocalDateTime.now().format(fmt);
    }

    private static long getRandomStr(long n) {
        long min = 1, max = 9;
        for (int i = 1; i < n; i++) {
            min *= 10;
            max *= 10;
        }
        long rangeLong = (((long) (new Random().nextDouble() * (max - min)))) + min;
        return rangeLong;
    }

    public String gen(int type) {
        switch (type) {
            case TYPE_GOODS:
                return CODE_GOODS + "-" + dateStr() + getRandomStr(4);

            case TYPE_GOODS_ITEMS:
                return CODE_GOODS_ITEMS + "-" + dateStr() + getRandomStr(4);

            case TYPE_APPROVAL:
                return CODE_APPROVAL + "-" + dateStr() + getRandomStr(4);

            case TYPE_PURCHANSE_STANDARD:
                return CODE_PURCHANSE_STANDARD + "-" + dateStr() + getRandomStr(4);

            case TYPE_PURCHANSE_PRESTORE:
                return CODE_PURCHANSE_PRESTORE + "-" + dateStr() + getRandomStr(4);

            case TYPE_PURCHANSE_PRESALE:
                return CODE_PURCHANSE_PRESALE + "-" + dateStr() + getRandomStr(4);

            case TYPE_PURCHANSE_NEW_DISTRIBUTOR:
                return CODE_PURCHANSE_NEW_DISTRIBUTOR + "-" + dateStr() + getRandomStr(4);

            case TYPE_PURCHANSE_CUSTOMIZE:
                return CODE_PURCHANSE_CUSTOMIZE + "-" + dateStr() + getRandomStr(4);

            case TYPE_MEMBER:
                return CODE_MEMBER + "-" + dateStr() + getRandomStr(4);

            case TYPE_SALE:
                return CODE_SALE + "-" + dateStr() + getRandomStr(4);

            case TYPE_STOCK_IN:
                return CODE_STOCK_IN + "-" + dateStr() + getRandomStr(4);

            case TYPE_STOCK_CHECK:
                return CODE_STOCK_CHECK + "-" + dateStr() + getRandomStr(4);

            case TYPE_ERP_STOCK_IN:
                return CODE_ERP_STOCK_IN + "-" + dateStr() + getRandomStr(4);

            case TYPE_ERP_STOCK_OUT:
                return CODE_ERP_STOCK_OUT + "-" + dateStr() + getRandomStr(4);

            case TYPE_ERP_STOCK_CHECK:
                return CODE_ERP_STOCK_CHECK + "-" + dateStr() + getRandomStr(4);

            case TYPE_TOP_UP:
                return CODE_TOP_UP + "-" + dateStr() + getRandomStr(4);

            case TYPE_FORM_EXCHANGE:
                return CODE_FORM_EXCHANGE + "-" + dateStr() + getRandomStr(4);

            case TYPE_SALE_RETURN:
                return CODE_SALE_RETURN + "-" + dateStr() + getRandomStr(4);

            case TYPE_SHOP:
                return CODE_SHOP + "-" + dateStr() + getRandomStr(4);

            case TYPE_BRIEFS_FORM:
                return CODE_BRIEFS_FORM + "-" + dateStr() + getRandomStr(4);

            case TYPE_REVERT_FORM:
                return CODE_REVERT_FORM + "-" + dateStr() + getRandomStr(4);

            case TYPE_SHAREHOLDER_CONTRACT:
                return CODE_SHAREHOLDER_CONTRACT + "-" + dateStr() + getRandomStr(4);

            case TYPE_Tm_SELF_EMPLOYED_LABORER:
                return CODE_Tm_SELF_EMPLOYED_LABORER + "-" + dateStr() + getRandomStr(4);

            case TYPE_LOGISTICS_DEPPON:
                return CODE_LOGISTICS_DEPPON + "-" + dateStr() + getRandomStr(4);

            case TYPE_LOGISTICS_OTHERS:
                return CODE_LOGISTICS_OTHERS + "-" + dateStr() + getRandomStr(4);

            case TYPE_PAY_FORM:
                return CODE_PAY_FORM + "-" + dateStr() + getRandomStr(4);

            case TYPE_PURCHANSE_SIXUE:
                return CODE_PURCHANSE_SIXUE + "-" + dateStr() + getRandomStr(4);

            case TYPE_PURCHANSE_SIXUE_PRESALE:
                return CODE_PURCHANSE_SIXUE_PRESALE + "-" + dateStr() + getRandomStr(4);

            case TYPE_Tm_PAY_FORM:
                return CODE_Tm_PAY_FORM + "-" + dateStr() + getRandomStr(4);

            case TYPE_SHAREHOLDER_CONTRACT_CANCEL:
                return CODE_SHAREHOLDER_CONTRACT_CANCEL + "-" + dateStr() + getRandomStr(4);

            default:
                return "";
        }
    }
}
