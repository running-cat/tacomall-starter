/***
 * @Author: 码上talk|RC/3189482282@qq.com
 * @Date: 2021-10-10 13:47:47
 * @LastEditTime: 2021-10-10 21:03:12
 * @LastEditors: 码上talk|RC
 * @Description: 
 * @FilePath: /tacomall-api/common/src/main/java/store/tacomall/common/entity/member/Member.java
 */
package store.tacomall.common.entity.member;

import java.time.LocalDateTime;

import lombok.Data;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.FieldFill;
import com.baomidou.mybatisplus.annotation.TableField;

@Data
public class Member {
    @TableId(value = "id", type = IdType.AUTO)
    private int id;

    private String username;

    private String passwd;

    private String mobile;

    private String inviteCode;

    private String inviteWxPic;

    @TableField(fill = FieldFill.INSERT)
    private int isDelete;

    @TableField(fill = FieldFill.INSERT)
    private LocalDateTime createTime;

    @TableField(fill = FieldFill.INSERT_UPDATE)
    private LocalDateTime updateTime;

    private LocalDateTime deleteTime;

}
