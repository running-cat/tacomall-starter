/***
 * @Author: 码上talk|RC
 * @Date: 2020-06-09 23:20:41
 * @LastEditTime: 2021-10-11 23:14:23
 * @LastEditors: 码上talk|RC
 * @Description: 
 * @FilePath: /tacomall-api/api/ma/src/main/java/store/tacomall/apima/controller/MemberController.java
 * @Just do what I think it is right
 */
package store.tacomall.apima.controller;

import com.alibaba.fastjson.JSONObject;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import store.tacomall.common.annotation.LoginUser;
import store.tacomall.apima.service.*;
import store.tacomall.common.entity.member.*;
import store.tacomall.common.json.ResponseJson;

@RestController
@RequestMapping(value = "/member/")
public class MemberController {

    @Autowired
    private MemberService memberService;

    @PostMapping("loginByMobile")
    public ResponseJson<String> loginByMobile(@RequestParam(value = "mobile") String username,
            @RequestParam(value = "mobile") String mobile) throws Exception {
        return memberService.loginByMobile(username, mobile);
    }

    @LoginUser
    @PostMapping("info")
    public ResponseJson<Member> info(@RequestBody JSONObject json) {
        return memberService.info(json);
    }

}
