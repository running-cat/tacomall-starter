/***
 * @Author: 码上talk|RC
 * @Date: 2020-07-13 11:06:56
 * @LastEditTime: 2021-10-10 20:59:44
 * @LastEditors: 码上talk|RC
 * @Description: 
 * @FilePath: /tacomall-api/api/ma/src/main/java/store/tacomall/apima/strategy/impl/PageGoodsStrategyImpl.java
 * @Just do what I think it is right
 */
package store.tacomall.apima.strategy.impl;

import java.util.HashMap;
import java.util.Map;

import com.alibaba.fastjson.JSONObject;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import store.tacomall.apima.strategy.PageStrategy;
import store.tacomall.common.json.ResponseJson;
import store.tacomall.common.mapper.goods.GoodsMapper;

@Component("pageGoods")
public class PageGoodsStrategyImpl implements PageStrategy {

    @Autowired
    private GoodsMapper goodsMapper;

    @Override
    public ResponseJson<Map<String, Object>> buildPage(JSONObject json) {
        ResponseJson<Map<String, Object>> responseJson = new ResponseJson<>();
        Map<String, Object> map = new HashMap<>();
        map.put("goods", null);
        map.put("recommendGoodsList", null);
        responseJson.setData(map);
        responseJson.ok();
        return responseJson;
    }
}
