/***
 * @Author: 码上talk|RC
 * @Date: 2020-07-10 16:59:34
 * @LastEditTime: 2020-11-14 11:06:20
 * @LastEditors: 码上talk|RC
 * @Description: 
 * @FilePath: /tacomall-api/tacomall-api/tacomall-api-ma/src/main/java/store/tacomall/apimashop/strategy/PageStrategy.java
 * @Just do what I think it is right
 */
package store.tacomall.apima.strategy;

import store.tacomall.common.json.ResponseJson;

import com.alibaba.fastjson.JSONObject;

import java.util.Map;

public interface PageStrategy {

  ResponseJson<Map<String, Object>> buildPage(JSONObject json);

}
