/***
 * @Author: 码上talk|RC
 * @Date: 2020-06-09 23:20:41
 * @LastEditTime: 2021-10-11 22:45:07
 * @LastEditors: 码上talk|RC
 * @Description: 
 * @FilePath: /tacomall-api/api/ma/src/main/java/store/tacomall/apima/ApiPortalApplication.java
 * @Just do what I think it is right
 */
package store.tacomall.apima;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.ComponentScan;
import org.mybatis.spring.annotation.MapperScan;
import org.springframework.context.annotation.FilterType;

import store.tacomall.common.config.RedisConfig;

@MapperScan({ "store.tacomall.common.mapper", "store.tacomall.apima.mapper" })
@SpringBootApplication(scanBasePackages = { "store.tacomall.common", "store.tacomall.apima" })
@ComponentScan(excludeFilters = {
        @ComponentScan.Filter(type = FilterType.ASSIGNABLE_TYPE, classes = { RedisConfig.class }) })
public class ApiPortalApplication {
    public static void main(String[] args) {
        SpringApplication.run(ApiPortalApplication.class, args);
    }

}
