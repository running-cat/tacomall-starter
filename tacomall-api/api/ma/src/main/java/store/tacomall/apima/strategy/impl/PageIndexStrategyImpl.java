/***
 * @Author: 码上talk|RC
 * @Date: 2020-07-10 17:00:09
 * @LastEditTime: 2021-10-10 21:00:58
 * @LastEditors: 码上talk|RC
 * @Description: 
 * @FilePath: /tacomall-api/api/ma/src/main/java/store/tacomall/apima/strategy/impl/PageIndexStrategyImpl.java
 * @Just do what I think it is right
 */
package store.tacomall.apima.strategy.impl;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import com.alibaba.fastjson.JSONObject;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Component;

import store.tacomall.apima.strategy.PageStrategy;
import store.tacomall.common.json.ResponseJson;

@Component("pageIndex")
public class PageIndexStrategyImpl implements PageStrategy {

    private final Logger logger = LoggerFactory.getLogger(this.getClass());

    @Override
    public ResponseJson<Map<String, Object>> buildPage(JSONObject json) {
        ResponseJson<Map<String, Object>> responseJson = new ResponseJson<>();
        Map<String, Object> map = new HashMap<>();
        List<Map<String, Object>> floor = new ArrayList<>();

        map.put("floor", floor);
        map.put("category", null);
        map.put("maBanner", null);
        responseJson.setData(map);
        responseJson.ok();
        return responseJson;
    }
}
