/**
 * @Author: 码上talk|RC
 * @Date: 2020-06-09 23:20:41
 * @LastEditTime: 2021-03-12 16:03:33
 * @LastEditors: 码上talk|RC
 * @Description: 
 * @FilePath: /tacomall-api/api/admin/src/main/java/cn/tacomall/apiadmin/controller/TmController.java
 * @Just do what I think it is right
 */
package store.tacomall.apiadmin.controller;

import java.util.List;

import com.alibaba.fastjson.JSONObject;

import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.beans.factory.annotation.Autowired;

import store.tacomall.common.entity.tm.TmStaff;
import store.tacomall.common.entity.tm.TmDept;
import store.tacomall.common.entity.tm.TmJob;
import store.tacomall.common.entity.tm.TmAccessRule;
import store.tacomall.common.json.ResponseJson;
import store.tacomall.common.json.ResponsePageJson;
import store.tacomall.apiadmin.annotation.TmLoginLog;
import store.tacomall.apiadmin.service.TmStaffService;
import store.tacomall.apiadmin.service.TmAccessRuleService;
import store.tacomall.apiadmin.service.TmDeptService;
import store.tacomall.apiadmin.service.TmJobService;

@RestController
@RequestMapping(value = "/tm/")
public class TmController {

  @Autowired
  private TmStaffService tmStaffService;

  @Autowired
  private TmAccessRuleService tmAccessRuleService;

  @Autowired
  private TmDeptService tmDeptService;

  @Autowired
  private TmJobService tmJobService;

  @TmLoginLog()
  @PostMapping("staffLogin")
  public ResponseJson<String> staffLogin(@RequestParam(value = "username") String username,
      @RequestParam(value = "passwd") String passwd) {
    return tmStaffService.login(username, passwd);
  }

  @PostMapping("staffLogout")
  public ResponseJson<String> logout() {
    return tmStaffService.logout();
  }

  @PostMapping("staffInfo")
  public ResponseJson<TmStaff> info(@RequestBody JSONObject json) {
    return tmStaffService.info(json);
  }

  @PostMapping("staffAccessRuleList")
  public ResponseJson<List<TmAccessRule>> rules() {
    return tmStaffService.accessRuleList();
  }

  @PostMapping("staffPage")
  public ResponsePageJson<List<TmStaff>> staffPage(@RequestParam(value = "pageIndex", defaultValue = "1") int pageIndex,
      @RequestParam(value = "pageSize", defaultValue = "10") int pageSize, @RequestBody JSONObject json) {
    return tmStaffService.page(pageIndex, pageSize, json);
  }

  @PostMapping("staffAdd")
  public ResponseJson<TmStaff> staffAdd(@RequestBody JSONObject json) {
    return tmStaffService.add(json);
  }

  @PostMapping("staffUpdate")
  public ResponseJson<TmStaff> staffUpdate(@RequestParam(value = "id") int id, @RequestBody JSONObject json) {
    return tmStaffService.update(id, json);
  }

  @PostMapping("deptPage")
  public ResponsePageJson<List<TmDept>> deptPage(@RequestParam(value = "pageIndex", defaultValue = "1") int pageIndex,
      @RequestParam(value = "pageSize", defaultValue = "10") int pageSize, @RequestBody JSONObject json) {
    return tmDeptService.page(pageIndex, pageSize, json);
  }

  @PostMapping("deptInfo")
  public ResponseJson<TmDept> deptInfo(@RequestBody JSONObject json) {
    return tmDeptService.info(json);
  }

  @PostMapping("deptAdd")
  public ResponseJson<TmDept> deptAdd(@RequestBody JSONObject json) {
    return tmDeptService.add(json);
  }

  @PostMapping("deptUpdate")
  public ResponseJson<TmDept> deptUpdate(@RequestParam(value = "id") int id, @RequestBody JSONObject json) {
    return tmDeptService.update(id, json);
  }

  @PostMapping("jobPage")
  public ResponsePageJson<List<TmJob>> jobPage(@RequestParam(value = "pageIndex", defaultValue = "1") int pageIndex,
      @RequestParam(value = "pageSize", defaultValue = "10") int pageSize, @RequestBody JSONObject json) {
    return tmJobService.page(pageIndex, pageSize, json);
  }

  @PostMapping("jobInfo")
  public ResponseJson<TmJob> jobInfo(@RequestBody JSONObject json) {
    return tmJobService.info(json);
  }

  @PostMapping("jobAdd")
  public ResponseJson<TmJob> jobAdd(@RequestBody JSONObject json) {
    return tmJobService.add(json);
  }

  @PostMapping("jobUpdate")
  public ResponseJson<TmJob> jobUpdate(@RequestParam(value = "id") int id, @RequestBody JSONObject json) {
    return tmJobService.update(id, json);
  }

  @PostMapping("accessRulePage")
  public ResponsePageJson<List<TmAccessRule>> accessRulePage(
      @RequestParam(value = "pageIndex", defaultValue = "1") int pageIndex,
      @RequestParam(value = "pageSize", defaultValue = "10") int pageSize, @RequestBody JSONObject json) {
    return tmAccessRuleService.page(pageIndex, pageSize, json);
  }

  @PostMapping("accessRuleList")
  public ResponseJson<List<TmAccessRule>> accessRulePage(@RequestBody JSONObject json) {
    return tmAccessRuleService.list(json);
  }

  @PostMapping("accessRuleInfo")
  public ResponseJson<TmAccessRule> accessRuleInfo(@RequestBody JSONObject json) {
    return tmAccessRuleService.info(json);
  }

  @PostMapping("accessRuleAdd")
  public ResponseJson<TmAccessRule> accessRuleAdd(@RequestBody JSONObject json) {
    return tmAccessRuleService.add(json);
  }

  @PostMapping("accessRuleUpdate")
  public ResponseJson<TmAccessRule> accessRuleUpdate(@RequestParam(value = "id") int id, @RequestBody JSONObject json) {
    return tmAccessRuleService.update(id, json);
  }

}
