/***
 * @Author: 码上talk|RC
 * @Date: 2020-06-09 23:20:41
 * @LastEditTime: 2021-10-10 14:28:38
 * @LastEditors: 码上talk|RC
 * @Description: 
 * @FilePath: /tacomall-api/api/admin/src/main/java/store/tacomall/apiadmin/service/TmStaffService.java
 * @Just do what I think it is right
 */
package store.tacomall.apiadmin.service;

import java.util.List;

import com.alibaba.fastjson.JSONObject;
import com.baomidou.mybatisplus.extension.service.IService;

import store.tacomall.common.entity.tm.TmAccessRule;
import store.tacomall.common.entity.tm.TmStaff;
import store.tacomall.common.json.ResponsePageJson;
import store.tacomall.common.json.ResponseJson;

public interface TmStaffService extends IService<TmStaff> {

    ResponseJson<String> login(String username, String password);

    ResponseJson<String> logout();

    ResponseJson<TmStaff> info(JSONObject json);

    ResponseJson<List<TmAccessRule>> accessRuleList();

    ResponsePageJson<List<TmStaff>> page(int pageIndex, int pageSize, JSONObject json);

    ResponseJson<TmStaff> add(JSONObject json);

    ResponseJson<TmStaff> update(int id, JSONObject json);
}
