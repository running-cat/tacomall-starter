/***
 * @Author: 码上talk|RC
 * @Date: 2021-01-18 15:01:39
 * @LastEditTime: 2021-10-10 14:23:58
 * @LastEditors: 码上talk|RC
 * @Description:
 * @FilePath: /tacomall-api/api/admin/src/main/java/store/tacomall/apiadmin/service/impl/TmAccessRuleServiceImpl.java
 * @微信:  13680065830
 * @邮箱:  3189482282@qq.com
 * @oops: Just do what I think it is right
 */
package store.tacomall.apiadmin.service.impl;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.stream.Collectors;

import cn.hutool.core.util.ObjectUtil;
import com.alibaba.fastjson.JSON;
import com.alibaba.fastjson.JSONObject;
import com.baomidou.mybatisplus.core.conditions.query.LambdaQueryWrapper;
import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.core.conditions.update.UpdateWrapper;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import org.springframework.stereotype.Service;

import store.tacomall.apiadmin.service.TmAccessRuleService;
import store.tacomall.common.entity.tm.TmAccessRule;
import store.tacomall.common.enumeration.BizEnum;
import store.tacomall.common.json.ResponseJson;
import store.tacomall.common.json.ResponsePageJson;
import store.tacomall.common.mapper.tm.TmAccessRuleMapper;
import store.tacomall.common.util.ExceptionUtil;

@Service
public class TmAccessRuleServiceImpl extends ServiceImpl<TmAccessRuleMapper, TmAccessRule>
        implements TmAccessRuleService {

    /***
     * @description: 权限分页
     * @param {type}
     * @return:
     */
    @Override
    public ResponsePageJson<List<TmAccessRule>> page(int pageIndex, int pageSize, JSONObject json) {
        ResponsePageJson<List<TmAccessRule>> responsePageVo = new ResponsePageJson<>();
        Page<TmAccessRule> page = new Page<>(pageIndex, pageSize);
        LambdaQueryWrapper<TmAccessRule> q = new QueryWrapper<TmAccessRule>().lambda();
        JSONObject query = json.getJSONObject("query");
        if (ObjectUtil.isNotEmpty(query) && ObjectUtil.isNotEmpty(query.get("keyword"))) {
            q.like(TmAccessRule::getName, query.get("keyword"));
        }
        if (ObjectUtil.isNotEmpty(query) && ObjectUtil.isNotEmpty(query.get("pId"))) {
            q.in(TmAccessRule::getPId, Arrays.asList(query.getString("pId").split(",")));
        }
        q.like(TmAccessRule::getIsDelete, 0);
        IPage<TmAccessRule> result = this.baseMapper.getTmAccessRulePage(page, q);
        responsePageVo.setData(result.getRecords());
        responsePageVo.buildPage(result.getCurrent(), result.getSize(), result.getTotal());
        responsePageVo.ok();
        return responsePageVo;
    }

    @Override
    public ResponseJson<List<TmAccessRule>> list(JSONObject json) {
        ResponseJson<List<TmAccessRule>> responseJson = new ResponseJson<>();
        JSONObject query = json.getJSONObject("query");
        LambdaQueryWrapper<TmAccessRule> lqw = new QueryWrapper<TmAccessRule>().lambda();
        if (ObjectUtil.isNotEmpty(query) && ObjectUtil.isNotEmpty(query.get("ids"))) {
            List<String> queryIds = new ArrayList<>();
            List<String> uniQueryIds = new ArrayList<>();
            this.baseMapper.selectList(new QueryWrapper<TmAccessRule>().lambda().in(TmAccessRule::getId,
                    Arrays.asList(query.getString("ids").split(",")))).stream().forEach(i -> {
                        Arrays.asList(i.getChainIds().split("<")).stream().forEach(j -> {
                            queryIds.add(j);
                        });
                        queryIds.add(String.valueOf(i.getId()));
                    });
            uniQueryIds = queryIds.stream().distinct().collect(Collectors.toList());
            lqw.in(TmAccessRule::getId, uniQueryIds);
        }
        lqw.like(TmAccessRule::getIsDelete, 0);
        responseJson.setData(this.baseMapper.selectList(lqw));
        responseJson.ok();
        return responseJson;
    }

    /***
     * @description: 权限详情
     * @param json
     * @return
     */
    @Override
    public ResponseJson<TmAccessRule> info(JSONObject json) {
        ResponseJson<TmAccessRule> responseJson = new ResponseJson<>();
        LambdaQueryWrapper<TmAccessRule> lqw = new QueryWrapper<TmAccessRule>().lambda();
        JSONObject query = json.getJSONObject("query");
        if (ObjectUtil.isNotEmpty(query)) {
            if (ObjectUtil.isNotEmpty(query.getInteger("id"))) {
                lqw.eq(TmAccessRule::getId, query.getInteger("id"));
            } else {
                ExceptionUtil.throwClientException(BizEnum.REQUEST_QUERY_UNMATCH.getMessage());
            }
        } else {
            ExceptionUtil.throwClientException(BizEnum.REQUEST_QUERY_REQUIRE.getMessage());
        }
        lqw.eq(TmAccessRule::getIsDelete, 0);

        responseJson.setData(this.baseMapper.selectOne(lqw));
        responseJson.ok();
        return responseJson;
    }

    /**
     * @description: 权限添加
     * @param jsonObject
     * @return
     */
    @Override
    public ResponseJson<TmAccessRule> add(JSONObject jsonObject) {
        ResponseJson<TmAccessRule> responseJson = new ResponseJson<>();
        TmAccessRule tmAccessRule = JSON.toJavaObject(jsonObject, TmAccessRule.class);
        this.baseMapper.insert(tmAccessRule);
        responseJson.ok();
        return responseJson;
    }

    /**
     * @description: 权限更新
     * @param id
     * @param json
     * @return
     */
    @Override
    public ResponseJson<TmAccessRule> update(int id, JSONObject json) {
        ResponseJson<TmAccessRule> responseJson = new ResponseJson<>();
        TmAccessRule tmAccessRule = JSON.toJavaObject(json, TmAccessRule.class);
        this.baseMapper.update(tmAccessRule, new UpdateWrapper<TmAccessRule>().lambda().eq(TmAccessRule::getId, id));
        responseJson.setData(tmAccessRule);
        responseJson.ok();
        return responseJson;
    }

}
