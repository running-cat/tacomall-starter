/*
 * @Author: 码上talk|RC/3189482282@qq.com
 * @Date: 2021-10-13 14:06:52
 * @LastEditTime: 2021-10-14 17:48:04
 * @LastEditors: 码上talk|RC
 * @Description: 
 * @FilePath: /tacomall-uniapp/vue.config.js
 */
let path = require('path');
let vars = path.resolve(__dirname, 'assets/less/var.less')
module.exports = {
  css: {
    loaderOptions: {
      less: {
        globalVars: {
          'hack': `true; @import "${vars}"`
        }
      }
    }
  },
  configureWebpack: {
    resolve: {
      alias: {
        vant: path.join(__dirname, 'wxcomponents/vant'),
        axqc: path.join(__dirname, 'components')

      }
    }
  }
}