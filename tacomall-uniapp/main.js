/*
 * @Author: 码上talk|RC
 * @Date: 2021-08-04 17:34:56
 * @LastEditTime: 2021-10-15 16:58:41
 * @LastEditors: 码上talk|RC
 * @Description: 
 * @FilePath: /tacomall-uniapp/main.js
 * @微信:  13680065830
 * @邮箱:  3189482282@qq.com
 * @oops: Just do what I think it is right
 */
import Vue from 'vue'
import App from './App'

import store from './store'
import { get, cloneDeep } from 'lodash'
import './mixins'

Vue.config.productionTip = false

Vue.prototype.$_ = {
  get,
  cloneDeep
}

App.mpType = 'app'

const app = new Vue({
  store,
  ...App
})
app.$mount()
