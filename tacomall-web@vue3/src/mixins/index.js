/*
 * @Author: 码上talk|RC
 * @Date: 2021-09-29 14:10:33
 * @LastEditTime: 2021-09-29 14:29:02
 * @LastEditors: 码上talk|RC
 * @Description:
 * @FilePath: /tacomall-web/src/mixins/index.js
 * @微信:  13680065830
 * @邮箱:  3189482282@qq.com
 * @oops: Just do what I think it is right
 */
import computed from './type/computed';
import filters from './type/filters';
import methods from './type/methods';

export function mixins (app) {
  app.mixin({
    computed
  });
  app.mixin({
    filters
  });
  app.mixin({
    methods
  });
}