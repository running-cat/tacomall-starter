/*
 * @Author: 码上talk|RC
 * @Date: 2021-09-29 14:11:29
 * @LastEditTime: 2021-09-29 14:31:18
 * @LastEditors: 码上talk|RC
 * @Description:
 * @FilePath: /tacomall-web/src/mixins/type/computed.js
 * @微信:  13680065830
 * @邮箱:  3189482282@qq.com
 * @oops: Just do what I think it is right
 */
import { mapState } from 'vuex'

export default {
  ...mapState(['isAdmin'])
}