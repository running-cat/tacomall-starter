/*
 * @Author: 码上talk|RC
 * @Date: 2021-09-29 14:10:52
 * @LastEditTime: 2021-09-29 14:56:58
 * @LastEditors: 码上talk|RC
 * @Description:
 * @FilePath: /tacomall-web/src/mixins/type/filters.js
 * @微信:  13680065830
 * @邮箱:  3189482282@qq.com
 * @oops: Just do what I think it is right
 */
import dayjs from 'dayjs';

export default {
  timeFormat (d, f = 'YYYY-MM-DD HH:mm') {
    if (!d) {
      return '1999-01-01 00:00';
    }
    return dayjs(d).format(f);
  },
  amountFormat (number, decimals = 2, decPoint, thousandsSep = ',') {
    number = (number + '').replace(/[^0-9+-Ee.]/g, '');
    const n = !isFinite(+number) ? 0 : +number;
    const prec = !isFinite(+decimals) ? 0 : Math.abs(decimals);
    const sep = (typeof thousandsSep === 'undefined') ? ',' : thousandsSep;
    const dec = (typeof decPoint === 'undefined') ? '.' : decPoint;
    let s = '';
    const toFixedFix = function (n, prec) {
      const k = Math.pow(10, prec);
      return '' + Math.ceil(n * k) / k;
    };

    s = (prec ? toFixedFix(n, prec) : '' + Math.round(n)).split('.');
    const re = /(-?\d+)(\d{3})/;
    while (re.test(s[0])) {
      s[0] = s[0].replace(re, '$1' + sep + '$2');
    }

    if ((s[1] || '').length < prec) {
      s[1] = s[1] || '';
      s[1] += new Array(prec - s[1].length + 1).join('0');
    }
    return s.join(dec);
  }
}