/*
 * @Author: 码上talk|RC
 * @Date: 2021-09-29 14:10:44
 * @LastEditTime: 2021-09-29 14:24:14
 * @LastEditors: 码上talk|RC
 * @Description:
 * @FilePath: /tacomall-web/src/mixins/type/methods.js
 * @微信:  13680065830
 * @邮箱:  3189482282@qq.com
 * @oops: Just do what I think it is right
 */
import dayjs from 'dayjs';

export default {
  getVal (s, k) {
    if (s instanceof Object) {
      return $_.get(s, k) || '--';
    }
    return s || '--';
  },
  isObjHasBlank (obj, skipAttr = []) {
    for (let i in obj) {
      if (!obj[i] && !skipAttr.includes(i)) {
        return true;
      }
    }
  },
  getYear (d) {
    return this.$dayjs(d).year();
  },
  getMonth (d) {
    return this.$dayjs(d).month() + 1;
  },
  getDayjsObj (v) {
    return dayjs(v);
  },
  getServerExceptionMsg (s) {
    if (!s) {
      return '服务端错误~'
    } else {
      const mk = s.split('BizException: ')
      return mk.length === 2 ? mk[1] : '服务端错误~'
    }
  }
}