/*
 * @Author: 码上talk|RC
 * @Date: 2021-09-28 16:18:41
 * @LastEditTime: 2021-09-28 16:21:29
 * @LastEditors: 码上talk|RC
 * @Description:
 * @FilePath: /tacomall-web/src/lib/log4js.js
 * @微信:  13680065830
 * @邮箱:  3189482282@qq.com
 * @oops: Just do what I think it is right
 */
const defaultConfig = {
  isUp2sever: false,
  url: ''
}
export class Log4js {
  config = {}

  constructor (config) {
    this.config = Object.assign(defaultConfig, config)
    this.log('log4js is ready');
  }

  debug (e) {
    const prefix = `%c
//     __  __     ____         _       __           __    __
//    / / / /__  / / /___     | |     / /___  _____/ /___/ /
//   / /_/ / _ \/ / / __ \    | | /| / / __ \/ ___/ / __  / 
//  / __  /  __/ / / /_/ /    | |/ |/ / /_/ / /  / / /_/ /  
// /_/ /_/\___/_/_/\____/     |__/|__/\____/_/  /_/\__,_/   
//                                             
      `
    const suffix = `%c
//
// =======================================================
//
      `
    console.log(prefix, 'color: blue')
    console.log(e)
    console.log(suffix, 'color: red')
  }

  log (e) {
    console.log(`%c ------------- power by log4js, log start -----------`, 'color: blue')
    console.log(e)
    console.log(`%c ------------- power by log4js, log end -----------`, 'color: red')
  }

  error (e) {
    console.log(`%c ------------- power by log4js, error start -----------`, 'color: blue')
    throw e
    console.log(`%c ------------- power by log4js, error end -----------`, 'color: red')
  }
}
