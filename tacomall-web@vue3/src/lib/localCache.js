/*
 * @Author: 码上talk|RC/3189482282@qq.com
 * @Date: 2021-09-28 16:35:03
 * @LastEditTime: 2021-10-14 16:52:53
 * @LastEditors: 码上talk|RC
 * @Description: 
 * @FilePath: /tacomall-web@vue3/src/lib/localCache.js
 */
import { appConfig } from '@/config/app';

export const localCache = {
  setToken: s => {
    localStorage.setItem(appConfig.tokenKey, s);
  },
  getToken: () => {
    return localStorage.getItem(appConfig.tokenKey);
  },
  clearToken: () => {
    localStorage.removeItem(appConfig.tokenKey);
  }
};
