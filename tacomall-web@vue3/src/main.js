/*
 * @Author: 码上talk|RC
 * @Date: 2021-09-28 15:55:25
 * @LastEditTime: 2021-09-29 17:54:26
 * @LastEditors: 码上talk|RC
 * @Description: 
 * @FilePath: /tacomall-web/src/main.js
 * @微信:  13680065830
 * @邮箱:  3189482282@qq.com
 * @oops: Just do what I think it is right
 */
import { createApp } from "vue";
import App from "./App.vue";
import { Log4js } from '@/lib/log4js';
import { router } from '@/router';
import { store } from '@/store';
import { mixins } from '@/mixins';
import '@/assets/less/reset.less';
import 'ant-design-vue/dist/antd.css';
import { antdv } from '@/config/antdv';
const { get, set, cloneDeep } = require('lodash');



const app = createApp(App);
app.config.globalProperties.$_ = window.$_ = {
  get,
  set,
  cloneDeep
}
app.config.globalProperties.$log4js = window.$dayjs = new Log4js();
app.use(router);
app.use(store);
mixins(app);
antdv.init(app);
app.mount("#app");
