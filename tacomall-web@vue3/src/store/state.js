/*
 * @Author: 码上talk|RC
 * @Date: 2021-09-29 14:00:35
 * @LastEditTime: 2021-09-29 14:05:09
 * @LastEditors: 码上talk|RC
 * @Description:
 * @FilePath: /tacomall-web/src/store/state.js
 * @微信:  13680065830
 * @邮箱:  3189482282@qq.com
 * @oops: Just do what I think it is right
 */
export const state = () => {
  return {
    isAdmin: false,
    version: '1.0.0'
  }
}