/*
 * @Author: 码上talk|RC
 * @Date: 2021-09-29 11:18:44
 * @LastEditTime: 2021-09-29 14:04:57
 * @LastEditors: 码上talk|RC
 * @Description:
 * @FilePath: /tacomall-web/src/store/actions.js
 * @微信:  13680065830
 * @邮箱:  3189482282@qq.com
 * @oops: Just do what I think it is right
 */
export const actions = {
  appInit: ({ commit }) => {
    commit('setIsAdmin', true);
  }
}