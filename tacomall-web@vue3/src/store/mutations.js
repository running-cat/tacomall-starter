/*
 * @Author: 码上talk|RC
 * @Date: 2021-09-29 11:18:36
 * @LastEditTime: 2021-09-29 14:30:51
 * @LastEditors: 码上talk|RC
 * @Description:
 * @FilePath: /tacomall-web/src/store/mutations.js
 * @微信:  13680065830
 * @邮箱:  3189482282@qq.com
 * @oops: Just do what I think it is right
 */
export const mutations = {
  setIsAdmin: (state, isAdmin) => {
    $_.set(state, 'isAdmin', isAdmin);
  }
}