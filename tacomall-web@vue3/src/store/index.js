/*
 * @Author: 码上talk|RC
 * @Date: 2021-09-29 11:18:22
 * @LastEditTime: 2021-09-29 14:30:55
 * @LastEditors: 码上talk|RC
 * @Description:
 * @FilePath: /tacomall-web/src/store/index.js
 * @微信:  13680065830
 * @邮箱:  3189482282@qq.com
 * @oops: Just do what I think it is right
 */
import { createStore } from 'vuex';

import { state } from './state';
import { getters } from './getters';
import { mutations } from './mutations';
import { actions } from './actions';


export const store = createStore({
  state,
  getters,
  mutations,
  actions
})