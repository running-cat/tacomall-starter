/*
 * @Author: 码上talk|RC
 * @Date: 2020-12-29 17:53:17
 * @LastEditTime: 2021-09-28 16:28:49
 * @LastEditors: 码上talk|RC
 * @Description:
 * @FilePath: /tacomall-web/src/model/entity/member/index.js
 * @微信:  13680065830
 * @邮箱:  3189482282@qq.com
 * @oops: Just do what I think it is right
 */

import Entity from '../../entity.class.js';
import options from '../../json/options/member';

const _tableField = {
  type: {
    type: 'int',
    default: 0
  },
  mobile: {
    type: 'string',
    default: ''
  }
};
class Member extends Entity {
  static _requestConfig = {
    app: 'shop',
    domain: 'member'
  }

  static _form = {}

  static options = options

  constructor (member) {
    super(member, { _tableField });
  }
}

export default Member;
