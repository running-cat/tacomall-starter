/*
 * @Author: 码上talk|RC
 * @Date: 2021-09-28 16:30:24
 * @LastEditTime: 2021-09-29 09:48:27
 * @LastEditors: 码上talk|RC
 * @Description:
 * @FilePath: /tacomall-web/src/model/entity/index.js
 * @微信:  13680065830
 * @邮箱:  3189482282@qq.com
 * @oops: Just do what I think it is right
 */
import Oss from './oss';
import Member from './member';
import Goods from './goods';
import GoodsItems from './goods/goodsItems';

export default {
  Oss,
  Member,
  Goods,
  GoodsItems
}