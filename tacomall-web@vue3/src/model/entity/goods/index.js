/*
 * @Author: 码上talk|RC
 * @Date: 2021-09-28 16:44:34
 * @LastEditTime: 2021-09-28 16:44:34
 * @LastEditors: 码上talk|RC
 * @Description:
 * @FilePath: /tacomall-web/src/model/entity/goods/index.js
 * @微信:  13680065830
 * @邮箱:  3189482282@qq.com
 * @oops: Just do what I think it is right
 */
import Entity from '../../entity.class.js';
import form from '../../json/form/goods';

const _tableField = {
  name: {
    type: 'int',
    default: 0
  },
  title: {
    type: 'string',
    default: ''
  }
};
class Goods extends Entity {
  static _requestConfig = {
    app: 'shop',
    domain: 'goods'
  }

  static _form = form;

  static options = {};

  constructor (goods) {
    super(goods, { _tableField });
  }
}

export default Goods;
