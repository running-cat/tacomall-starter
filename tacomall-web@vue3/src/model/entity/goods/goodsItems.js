/*
 * @Author: 码上talk|RC
 * @Date: 2021-09-28 16:44:34
 * @LastEditTime: 2021-09-29 09:48:01
 * @LastEditors: 码上talk|RC
 * @Description:
 * @FilePath: /tacomall-web/src/model/entity/goods/goodsItems.js
 * @微信:  13680065830
 * @邮箱:  3189482282@qq.com
 * @oops: Just do what I think it is right
 */
import Entity from '../../entity.class.js';
import form from '../../json/form/goods/goodsItems';

const _tableField = {
  name: {
    type: 'int',
    default: 0
  },
  amount: {
    type: 'int',
    default: 0
  }
};
class GoodsItems extends Entity {
  static _requestConfig = {
    app: 'shop',
    domain: 'goodsItems'
  }

  static _form = form;

  static options = {};

  constructor (goodsItems) {
    super(goodsItems, { _tableField });
  }
}

export default GoodsItems;
