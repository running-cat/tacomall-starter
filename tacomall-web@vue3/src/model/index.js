/*
 * @Author: 码上talk|RC
 * @Date: 2021-04-22 11:34:59
 * @LastEditTime: 2021-09-28 16:31:35
 * @LastEditors: 码上talk|RC
 * @Description:
 * @FilePath: /tacomall-web/src/model/index.js
 * @微信:  13680065830
 * @邮箱:  3189482282@qq.com
 * @oops: Just do what I think it is right
 */
import { Request } from './request';
import entity from './entity';

export const model = {
  entity,
  Request
};
