/*
 * @Author: 码上talk|RC
 * @Date: 2021-09-28 16:29:37
 * @LastEditTime: 2021-09-28 16:54:10
 * @LastEditors: 码上talk|RC
 * @Description:
 * @FilePath: /tacomall-web/src/config/app.js
 * @微信:  13680065830
 * @邮箱:  3189482282@qq.com
 * @oops: Just do what I think it is right
 */
export const appConfig = {
  tokenKey: 'x-access-token',
  ossUrl: {
    dev: '',
    test: '',
    prod: ''
  }
};
