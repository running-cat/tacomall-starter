/*
 * @Author: 码上talk|RC
 * @Date: 2021-09-29 17:41:02
 * @LastEditTime: 2021-09-29 17:55:12
 * @LastEditors: 码上talk|RC
 * @Description:
 * @FilePath: /tacomall-web/src/config/antdv.js
 * @微信:  13680065830
 * @邮箱:  3189482282@qq.com
 * @oops: Just do what I think it is right
 */
import { Button, message } from 'ant-design-vue';

export const antdv = {
  init: (app) => {
    [Button].forEach(cpn => {
      app.use(cpn);
    });
    app.config.globalProperties.$message = message;
  }
}