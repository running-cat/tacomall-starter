/*
 * @Author: 码上talk|RC
 * @Date: 2021-09-29 10:50:02
 * @LastEditTime: 2021-09-29 11:16:08
 * @LastEditors: 码上talk|RC
 * @Description:
 * @FilePath: /tacomall-web/src/router/index.js
 * @微信:  13680065830
 * @邮箱:  3189482282@qq.com
 * @oops: Just do what I think it is right
 */
import { createRouter, createWebHistory } from 'vue-router'
import { routesInit } from './routes/init';


export const router = createRouter({
  history: createWebHistory(),
  routes: [...routesInit]
});