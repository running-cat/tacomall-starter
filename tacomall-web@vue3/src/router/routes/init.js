/*
 * @Author: 码上talk|RC
 * @Date: 2021-09-29 10:58:15
 * @LastEditTime: 2021-10-13 13:42:49
 * @LastEditors: 码上talk|RC
 * @Description:
 * @FilePath: /tacomall-web/src/router/routes/init.js
 * @微信:  13680065830
 * @邮箱:  3189482282@qq.com
 * @oops: Just do what I think it is right
 */
export const routesInit = [
  {
    name: '/',
    path: '/',
    redirect: '/index'
  },
  {
    name: 'layoutDefault',
    path: '/layout-default',
    component: () => import('@/layout/default'),
    children: [
      {
        name: 'login',
        path: '/login',
        meta: {
          title: '登录',
          requiresAuth: false
        },
        component: () => import('@/pages/login')
      }
    ]
  },
  {
    name: 'layoutAuth',
    path: '/layout-auth',
    component: () => import('@/layout/auth'),
    children: [
      {
        name: 'index',
        path: '/index',
        meta: {
          title: '首页',
          requiresAuth: true
        },
        component: () => import('@/pages/index')
      }
    ]
  }
];