/*
 * @Author: 码上talk|RC
 * @Date: 2021-05-04 15:30:39
 * @LastEditTime: 2021-10-03 13:58:16
 * @LastEditors: 码上talk|RC
 * @Description: 
 * @FilePath: /server-ws/app.js
 * @微信:  13680065830
 * @邮箱:  3189482282@qq.com
 * @oops: Just do what I think it is right
 */
const Koa = require('koa')
const app = new Koa()
const views = require('koa-views')
const json = require('koa-json')
const onerror = require('koa-onerror')
const bodyparser = require('koa-bodyparser')
const logger = require('koa-logger')
var cors = require('koa-cors');
let log4js = require('log4js')

const index = require('./routes/index')
const axq = require('./routes/axq')
const sys = require('./routes/sys')

log4js.configure({
  appenders: {
    file: {
      type: 'file',
      filename: './.logs/app.log',
      layout: {
        type: 'pattern',
        pattern: '%r %p - %m'
      }
    }
  },
  categories: {
    default: {
      appenders: ['file'],
      level: 'debug'
    }
  }
})

// error handler
onerror(app)

// middlewares
app.use(bodyparser({
  enableTypes: ['json', 'form', 'text']
}))
app.use(json())
app.use(logger())
app.use(require('koa-static')(__dirname + '/public'))

app.use(views(__dirname + '/views', {
  extension: 'ejs'
}))

// logger
app.use(async (ctx, next) => {
  const start = new Date()
  await next()
  const ms = new Date() - start
  console.log(`${ctx.method} ${ctx.url} - ${ms}ms`)
})

// cors
app.use(cors())

// routes
app.use(index.routes(), index.allowedMethods())
app.use(axq.routes(), axq.allowedMethods())
app.use(sys.routes(), sys.allowedMethods())

// error-handling
app.on('error', (err, ctx) => {
  console.error('server error', err, ctx)
});

module.exports = app
