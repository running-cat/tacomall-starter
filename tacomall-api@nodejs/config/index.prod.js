/*
 * @Author: 码上talk|RC
 * @Date: 2021-06-03 22:04:47
 * @LastEditTime: 2021-10-15 17:19:52
 * @LastEditors: 码上talk|RC
 * @Description:
 * @FilePath: /tacomall-api@nodejs/config/index.prod.js
 * @微信:  13680065830
 * @邮箱:  3189482282@qq.com
 * @oops: Just do what I think it is right
 */
const config = {
  mysql: {
    DATABASE: '',
    USERNAME: '',
    PASSWORD: '',
    PORT: '',
    HOST: ''
  }
}

module.exports = config