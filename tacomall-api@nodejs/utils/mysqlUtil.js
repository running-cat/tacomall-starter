/*
 * @Author: 码上talk|RC
 * @Date: 2021-06-03 22:04:47
 * @LastEditTime: 2021-09-20 00:33:05
 * @LastEditors: 码上talk|RC
 * @Description: 
 * @FilePath: /server-ws/utils/mysqlUtil.js
 * @微信:  13680065830
 * @邮箱:  3189482282@qq.com
 * @oops: Just do what I think it is right
 */
var mysql = require('mysql')
var argvUtil = require('./argvUtil')
var config = require('../config')

if (argvUtil.get('env') === 'test') {
    var config = require('../config/index.test')
}

if (argvUtil.get('env') === 'prod') {
    var config = require('../config/index.prod')
}


var pool = mysql.createPool({
    host: config.mysql.HOST,
    port: config.mysql.PORT,
    user: config.mysql.USERNAME,
    password: config.mysql.PASSWORD,
    database: config.mysql.DATABASE
})

var mysqlUtil = {
    query: (sql, values) => {
        console.log(sql)
        return new Promise((resolve, reject) => {
            pool.getConnection((err, connection) => {
                if (err) {
                    reject(err)
                } else {
                    connection.query(sql, values, (err, rows) => {
                        if (err) {
                            reject(err)
                        } else {
                            resolve(rows)
                        }
                        connection.release()
                    })
                }
            })
        })
    }
}

module.exports = mysqlUtil