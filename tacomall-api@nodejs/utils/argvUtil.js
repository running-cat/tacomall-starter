/*
 * @Author: 码上talk|RC
 * @Date: 2021-06-03 22:04:47
 * @LastEditTime: 2021-08-03 20:22:28
 * @LastEditors: 码上talk|RC
 * @Description: 
 * @FilePath: /server-ws/utils/argvUtil.js
 * @微信:  13680065830
 * @邮箱:  3189482282@qq.com
 * @oops: Just do what I think it is right
 */
module.exports = {
  get: (s) => {
    if (!process.env.npm_config_argv) {
      return 'dev'
    }
    var argv = JSON.parse(process.env.npm_config_argv).cooked
    var sIndex = argv.indexOf(`--${s}`)
    if (sIndex === -1) {
      return ''
    }
    return argv[sIndex + 1]
  }
}