/*
 * @Author: 码上talk|RC
 * @Date: 2021-09-11 14:06:46
 * @LastEditTime: 2021-09-15 22:06:11
 * @LastEditors: 码上talk|RC
 * @Description:
 * @FilePath: /server-ws/socket/auth.js
 * @微信:  13680065830
 * @邮箱:  3189482282@qq.com
 * @oops: Just do what I think it is right
 */
var mysqlUtil = require('../utils/mysqlUtil')

module.exports = (io, socket, callback) => {
  var { query } = socket.handshake
  var { appid, secret } = query
  if (!appid || !secret) {
    socket.disconnect(true)
  }
  mysqlUtil.query(`SELECT * FROM sys_ws_app WHERE appid = '${appid}' AND secret = '${secret}' AND is_delete = 0`).then(rows => {
    if (!rows.length) {
      socket.disconnect(true)
      socket.emit(`login-status`, { status: false, message: 'appid verify fail' })
    }
    socket.emit(`login-status`, { status: true, message: 'appid verify success' })
    callback && callback()
  })
}