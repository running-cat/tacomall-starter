/*
 * @Author: 码上talk|RC
 * @Date: 2021-09-15 16:52:53
 * @LastEditTime: 2021-10-15 17:20:41
 * @LastEditors: 码上talk|RC
 * @Description:
 * @FilePath: /tacomall-api@nodejs/socket/eventHandler.js
 * @微信:  13680065830
 * @邮箱:  3189482282@qq.com
 * @oops: Just do what I think it is right
 */
let log4js = require('log4js')
let logger = log4js.getLogger()
var mysqlUtil = require('../utils/mysqlUtil')

module.exports = (io, socket) => {
  socket.on('disconnect', () => {
    console.log('a user disconnected')
  })

  socket.on('join', (room) => {
    socket.join(room)
    let r = io.of('/').adapter.rooms.get(room)
    let userSize = r.size
    logger.log(`the number if user in room is: ${userSize}`)
    if (userSize < 300) {
      socket.emit('joined', room, socket.id)
      if (userSize > 1) {
        socket.to(room).emit('joined', room, socket.id)
      }
    } else {
      socket.leave(room)
      socket.emit('full', room, socket.id)
    }
  })

  socket.on('leave', (room) => {
    let r = io.of('/').adapter.rooms.get(room)
    let userSize = r.size
    logger.log(`the number if user in room is: ${userSize - 1}`)
    socket.leave(room)
    socket.to(room).emit('leaved', room, socket.id)
    socket.emit('leaved', room, socket.id)
  })
}