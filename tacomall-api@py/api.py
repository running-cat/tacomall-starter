'''
@Author: 码上talk|RC
@Date: 2020-06-09 23:21:49
LastEditTime: 2021-08-10 13:42:40
LastEditors: 码上talk|RC
@Description: 
FilePath: /server-py/api.py
@Just do what I think it is right
'''
from sanic import Blueprint, Sanic
from sanic_cors import CORS

from module.api.routes.file import file_blueprint

app = Sanic(__name__)
CORS(app)

app.blueprint(file_blueprint)


if __name__ == '__main__':
    app.run(host="0.0.0.0", port=8100, debug=True)
