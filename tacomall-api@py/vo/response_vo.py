'''
Author: 码上talk|RC
Date: 2020-06-09 23:21:49
LastEditTime: 2021-06-09 14:42:31
LastEditors: 码上talk|RC
Description:
FilePath: /server-py/vo/response_vo.py
微信:  13680065830
邮箱:  3189482282@qq.com
oops: Just do what I think it is right
'''
from sanic.response import file, json


class ResponseVo():
    def __init__(self):
        self.success = True
        self.code = 1000
        self.data = {}
        self.page = None

    def set_success(self, success):
        self.success = success

    def set_code(self, code):
        self.code = code

    def set_data(self, data):
        self.data = data

    def set_page(self, page):
        self.page = page

    def json(self):
        if self.page is not None:
            return json({
                'success': self.success,
                'code': self.code,
                'data': self.data,
                'page': self.page
            })
        return json({
            'success': self.success,
            'code': self.code,
            'data': self.data
        })
