'''
Author: 码上talk|RC
Date: 2020-09-24 09:42:39
LastEditTime: 2021-10-15 17:25:01
LastEditors: 码上talk|RC
Description: 
FilePath: /tacomall-api@py/module/api/routes/file.py
微信:  13680065830
邮箱:  3189482282@qq.com
oops: Just do what I think it is right
'''
from sanic import Blueprint, Sanic
from sanic.response import file, json

file_blueprint = Blueprint('file', url_prefix='/file')


@file_blueprint.route('/test')
async def test(request):
    return json({'msg': 'hi from blueprint'})
