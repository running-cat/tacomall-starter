'''
Author: 码上talk|RC
Date: 2021-06-09 14:26:47
LastEditTime: 2021-06-09 14:35:00
LastEditors: 码上talk|RC
Description: 
FilePath: /server-py/config/index.py
微信:  13680065830
邮箱:  3189482282@qq.com
oops: Just do what I think it is right
'''
import configparser
from utils.path_util import PathUtil
from utils.argv_util import argv_util


def get_config():
    cf = configparser.ConfigParser()
    cf.read(PathUtil.root('config/env/') +
            'app.{0}.cfg'.format(argv_util('env')))
    return cf
