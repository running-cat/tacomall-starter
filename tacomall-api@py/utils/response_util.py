from flask import jsonify


class ResponseUtil():

    @staticmethod
    def json(responseVo):
        return jsonify(responseVo)
