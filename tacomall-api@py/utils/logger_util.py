'''
Author: 码上talk|RC
Date: 2020-08-05 21:01:28
LastEditTime: 2021-08-16 13:48:26
LastEditors: 码上talk|RC
Description: 
FilePath: /server-py/utils/logger_util.py
微信:  13680065830
邮箱:  3189482282@qq.com
oops: Just do what I think it is right
'''


def logger4py(m, color='\033[32m', log2file=''):
    print('{color}===>>>{m}'.format(m=m, color=color))
    print('\033[0m')
    if log2file:
        with open(log2file, 'a') as f:
            f.write(m + '\n')
