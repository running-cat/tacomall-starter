'''
Author: 码上talk|RC
Date: 2020-09-24 11:47:15
LastEditTime: 2021-08-16 10:16:35
LastEditors: 码上talk|RC
Description: 
FilePath: /server-py/utils/ws_client_util.py
微信:  13680065830
邮箱:  3189482282@qq.com
oops: Just do what I think it is right
'''
import socketio


class WsClientUtil():
    def __init__(self, config={'url': 'http://127.0.0.1:8200'}):
        self.config = config
        self.sio = None

    def connect(self):
        self.sio = socketio.Client()
        self.sio.connect(
            self.config['url'] + '?appid=sdfedc&secret=jlsdjif462dfgjlkjsd', headers={})

    def send(self, topic, msg):
        self.sio.emit(topic, msg)

    def disconnect(self):
        self.sio.disconnect()
