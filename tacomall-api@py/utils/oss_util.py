'''
Author: 码上talk|RC
Date: 2020-06-09 23:21:49
LastEditTime: 2021-06-19 17:05:03
LastEditors: 码上talk|RC
Description: 
FilePath: /server-py/utils/oss_util.py
微信:  13680065830
邮箱:  3189482282@qq.com
oops: Just do what I think it is right
'''
import os
import oss2
import uuid

from config.index import get_config


class OssUtil():
    def __init__(self, bucket='bucketImg'):
        self.cf = get_config()
        self.bucketStr = bucket
        auth = oss2.Auth(self.cf.get('oss', 'accesskey'),
                         self.cf.get('oss', 'accessSecret'))
        self.bucket = oss2.Bucket(auth, self.cf.get(
            'oss', 'endpoint'), self.cf.get('oss', bucket))

    def upload_local_file(self, filename):
        key = str(uuid.uuid4())
        self.bucket.put_object_from_file(key, filename)
        return {
            'url': '//' + self.cf.get('oss', self.bucketStr) + '.' + self.cf.get('oss', 'endpoint') + '/' + key,
            'key': key
        }

    def upload_bytes(self, bytes):
        key = str(uuid.uuid4())
        self.bucket.put_object(key, bytes)
        return {
            'url': '//' + self.cf.get('oss', self.bucketStr) + '.' + self.cf.get('oss', 'endpoint') + '/' + key,
            'key': key
        }
