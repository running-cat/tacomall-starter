'''
Author: 码上talk|RC
Date: 2021-05-04 16:15:52
LastEditTime: 2021-06-09 16:16:18
LastEditors: 码上talk|RC
Description: 
FilePath: /server-py/utils/request_util.py
微信:  13680065830
邮箱:  3189482282@qq.com
oops: Just do what I think it is right
'''


class RequestUtil():

    @staticmethod
    def get(request, key):
        try:
            return request.args[key][0]
        except:
            raise Exception("params {0} could not be null".format(key))
