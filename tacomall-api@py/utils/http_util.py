import requests
from requests.adapters import HTTPAdapter

class HttpUtil():

    def __init__(self, base_url):
        self.base_url = base_url

    def get(self, url):
        s = requests.Session()
        s.mount('http://',HTTPAdapter(max_retries=3))
        s.mount('https://',HTTPAdapter(max_retries=3))
        try:
            return requests.get(self.base_url + url)
        except requests.exceptions.ConnectionError as e:
            print('连接失败，该URL可能被墙掉了')
                

    def post(self, url, data):
        resp = requests.post(self.base_url + url, data).json()
        if resp['success'] is True:
            return resp['data']