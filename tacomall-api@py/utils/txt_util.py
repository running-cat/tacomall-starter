'''
Author: 码上talk|RC
Date: 2020-09-11 14:13:30
LastEditTime: 2021-06-19 17:45:59
LastEditors: 码上talk|RC
Description:
FilePath: /server-py/utils/txt_util.py
微信:  13680065830
邮箱:  3189482282@qq.com
oops: Just do what I think it is right
'''
import time
import pandas as pd
from copy import deepcopy

from utils.path_util import PathUtil
from utils.oss_util import OssUtil


class TxtUtil():
    def __init__(self, config={}):
        self._default_config = {}
        self._init_time = time.time()
        self.config = {}
        self._init_config(config)
        self.file_path = PathUtil.root(
            '.tmp/txt/downlaod/') + '{filename}.txt'.format(filename=self._init_time)

    def _init_config(self, config):
        self.config = deepcopy(self._default_config)
        self.config.update(config)

    def writelines(self, lst):
        with open(self.file_path, "w") as f:
            f.writelines(lst)

    def save(self, config={'is_upload_to_oss': True}):
        result = {}
        if ('is_upload_to_oss' in config):
            oss_util = OssUtil(bucket='bucketFile')
            result['oss_url'] = oss_util.upload_local_file(self.file_path)[
                'url']
        return result
