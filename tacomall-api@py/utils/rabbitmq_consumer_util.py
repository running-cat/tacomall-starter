'''
Author: 码上talk|RC
Date: 2020-09-24 10:26:02
LastEditTime: 2021-07-23 19:49:39
LastEditors: 码上talk|RC
Description: 
FilePath: /server-py/utils/rabbitmq_consumer_util.py
微信:  13680065830
邮箱:  3189482282@qq.com
oops: Just do what I think it is right
'''

import pika

from utils.logger_util import logger4py


class RabbitmqConsumerUtil():

    def __init__(self, config={
        'user': 'guest',
        'passwd': 'guest',
        'ip': '127.0.0.1',
        'port': '7001',
        'virtual_host': '/'
    }):
        self.config = config
        self.connection = None
        self.channel = None
        self.routing_key = ''
        self._connect()

    def _connect(self):
        credentials = pika.PlainCredentials(
            self.config['user'], self.config['passwd'])
        self.connection = pika.BlockingConnection(pika.ConnectionParameters(
            host=self.config['ip'], port=self.config['port'], virtual_host=self.config['virtual_host'], credentials=credentials))

    def consuming(self, exchange, queue, routing, callback):
        channel = self.connection.channel()
        channel.exchange_declare(exchange=exchange)
        channel.queue_declare(queue=queue)
        channel.queue_bind(queue, exchange, routing)
        channel.basic_consume(queue, callback)
        channel.start_consuming()
