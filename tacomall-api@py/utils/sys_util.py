'''
@Author: 码上talk|RC
@Date: 2020-06-09 23:21:49
@LastEditTime: 2020-07-08 16:02:56
@LastEditors: 码上talk|RC
@Description: 
@FilePath: /charis-api/utils/sys_util.py
@Just do what I think it is right
'''
import uuid
import platform
import subprocess


class SysUtil():
    @staticmethod
    def get_mac_address():
        mac = uuid.UUID(int=uuid.getnode()).hex[-12:]
        return ":".join([mac[e:e+2] for e in range(0, 11, 2)])

    @staticmethod
    def execute_shell(cmd):
        popen = subprocess.Popen(cmd, stdout=subprocess.PIPE,
                                 stderr=subprocess.PIPE, shell=True)
        for i in iter(popen.stdout.readline, ''):
            if len(i) < 1:
                break
            print(i.decode('gbk').strip())

    @staticmethod
    def is_windows():
        return platform.system() == 'Windows'

    @staticmethod
    def _print(m, color='\033[32m'):
        print('{color}===>>>{m}'.format(m=m, color=color))
        print('\033[0m')
