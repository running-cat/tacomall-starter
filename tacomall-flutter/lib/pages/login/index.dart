/*
 * @Author: 码上talk|RC
 * @Date: 2020-08-24 09:53:15
 * @LastEditTime: 2020-08-27 09:06:30
 * @LastEditors: 码上talk|RC
 * @Description: 
 * @FilePath: /tacomall-flutter/lib/pages/login/index.dart
 * @微信:  13680065830
 * @邮箱:  3189482282@qq.com
 * @oops: Just do what I think it is right
 */
import 'package:flutter/material.dart';
import 'package:color_dart/color_dart.dart';

import 'package:tacomall_flutter/common/global.dart';
import 'package:tacomall_flutter/ui/widget/button/index.dart';

class Login extends StatefulWidget {
  static _LoginState _loginState;

  Login() {
    _loginState = _LoginState();
  }

  getAppBar() => _loginState.createAppBar();

  _LoginState createState() => _LoginState();
}

class _LoginState extends State<Login> {
  static Map formAccount = {'value': null, 'verify': true};
  static Map formPasswd = {'value': null, 'verify': true};

  AppBar createAppBar() {
    return null;
  }

  login() {
    if (!formAccount['verify'] || formAccount['value'] == null) {
      return G.toast('账号格式错误');
    }
    G.routerPush('/');
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
        body: Container(
            alignment: Alignment.center,
            child: Container(
                width: G.screenUtil().setWidth(540),
                margin:
                    EdgeInsets.fromLTRB(0, G.screenUtil().setHeight(300), 0, 0),
                child: Column(
                  children: <Widget>[
                    Container(
                      alignment: Alignment.centerLeft,
                      padding: EdgeInsets.fromLTRB(
                          0, 0, 0, G.screenUtil().setHeight(40)),
                      child: Text(
                        '账号登录',
                        style: TextStyle(
                          fontSize: G.screenUtil().setWidth(36),
                          fontWeight: FontWeight.bold,
                        ),
                      ),
                    ),
                    Container(
                      height: G.screenUtil().setWidth(80),
                      padding: EdgeInsets.fromLTRB(G.screenUtil().setWidth(30),
                          0, G.screenUtil().setWidth(30), 0),
                      decoration: BoxDecoration(
                          color: rgba(244, 244, 244, 1),
                          borderRadius: BorderRadius.circular(
                              G.screenUtil().setWidth(5))),
                      child: TextField(
                          decoration: InputDecoration(
                              counterText: "",
                              border: InputBorder.none,
                              hintText: '请输入账号',
                              hintStyle: TextStyle(
                                fontSize: G.screenUtil().setWidth(32),
                              )),
                          onChanged: (e) {
                            RegExp regExp = RegExp('^[a-zA-Z0-9_-]+');
                            setState(() {
                              formAccount['value'] = e;
                              formAccount['verify'] = regExp.hasMatch(e);
                            });
                          }),
                    ),
                    Container(
                      height: G.screenUtil().setWidth(80),
                      padding: EdgeInsets.fromLTRB(G.screenUtil().setWidth(30),
                          0, G.screenUtil().setWidth(30), 0),
                      margin: EdgeInsets.fromLTRB(
                          0, G.screenUtil().setWidth(20), 0, 0),
                      decoration: BoxDecoration(
                          color: rgba(244, 244, 244, 1),
                          borderRadius: BorderRadius.circular(
                              G.screenUtil().setWidth(5))),
                      child: TextField(
                          decoration: InputDecoration(
                              counterText: '',
                              border: InputBorder.none,
                              hintText: '请输入密码',
                              hintStyle: TextStyle(
                                fontSize: G.screenUtil().setWidth(32),
                              )),
                          onChanged: (e) {
                            RegExp regExp = RegExp('^[a-zA-Z0-9_-]+');
                            setState(() {
                              formPasswd['value'] = e;
                              formPasswd['verify'] = regExp.hasMatch(e);
                            });
                          }),
                    ),
                    Container(
                      margin: EdgeInsets.fromLTRB(
                          0, G.screenUtil().setWidth(20), 0, 0),
                      child: TCButton.normal(
                          width: G.screenUtil().setWidth(540),
                          height: G.screenUtil().setWidth(80),
                          child: Text('登录'),
                          bgColor: rgba(0, 127, 255, 1),
                          color: hex('#fff'),
                          onPressed: () => login()),
                    ),
                    Container(
                      margin: EdgeInsets.fromLTRB(
                          0, G.screenUtil().setWidth(20), 0, 0),
                      child: Row(
                        mainAxisAlignment: MainAxisAlignment.spaceBetween,
                        children: <Widget>[
                          Container(
                            child: Container(
                                child: Row(
                              children: <Widget>[
                                Container(
                                  child: Text('没有账号？'),
                                ),
                                Container(
                                  child: Text('注册',
                                      style: TextStyle(
                                        fontSize: G.screenUtil().setWidth(32),
                                        color: rgba(0, 127, 255, 1),
                                      )),
                                )
                              ],
                            )),
                          ),
                          Container(
                            child: Text('忘记密码'),
                          )
                        ],
                      ),
                    )
                  ],
                ))));
  }
}
