/*
 * @Author: 码上talk|RC
 * @Date: 2020-06-18 11:51:10
 * @LastEditTime: 2020-08-27 09:13:54
 * @LastEditors: 码上talk|RC
 * @Description: 
 * @FilePath: /tacomall-flutter/lib/pages/index/cart/index.dart
 * @Just do what I think it is right
 */
import 'package:flutter/material.dart';

class Cart extends StatefulWidget {
  static _CartState _cartState;

  Cart() {
    _cartState = _CartState();
  }

  getAppBar() => _cartState.createAppBar();

  _CartState createState() => _CartState();
}

class _CartState extends State<Cart> {
  AppBar createAppBar() {
    return null;
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(body: Container(child: Center(child: Text("购物车"))));
  }
}
