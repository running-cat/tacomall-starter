/*
 * @Author: 码上talk|RC
 * @Date: 2020-06-18 11:51:24
 * @LastEditTime: 2020-08-27 09:24:27
 * @LastEditors: 码上talk|RC
 * @Description: 
 * @FilePath: /tacomall-flutter/lib/pages/index/mine/index.dart
 * @Just do what I think it is right
 */
import 'package:flutter/material.dart';

class Mine extends StatefulWidget {
  static _MineState _mineState;

  Mine() {
    _mineState = _MineState();
  }

  getAppBar() => _mineState.createAppBar();

  _MineState createState() => _MineState();
}

class _MineState extends State<Mine> {
  AppBar createAppBar() {
    return null;
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(body: Container(child: Center(child: Text("个人中心"))));
  }
}
