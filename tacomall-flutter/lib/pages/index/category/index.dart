/*
 * @Author: 码上talk|RC
 * @Date: 2020-06-18 11:51:17
 * @LastEditTime: 2020-08-27 09:13:39
 * @LastEditors: 码上talk|RC
 * @Description: 
 * @FilePath: /tacomall-flutter/lib/pages/index/category/index.dart
 * @Just do what I think it is right
 */
import 'package:flutter/material.dart';

class Category extends StatefulWidget {
  static _CategoryState _categoryState;

  Category() {
    _categoryState = _CategoryState();
  }

  getAppBar() => _categoryState.createAppBar();

  _CategoryState createState() => _CategoryState();
}

class _CategoryState extends State<Category> {
  AppBar createAppBar() {
    return null;
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(body: Container(child: Center(child: Text("分类"))));
  }
}
