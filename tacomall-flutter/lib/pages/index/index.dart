/*
 * @Author: 码上talk|RC
 * @Date: 2020-06-18 08:55:58
 * @LastEditTime: 2020-08-25 09:25:23
 * @LastEditors: 码上talk|RC
 * @Description: 
 * @FilePath: /tacomall-flutter/lib/pages/index/index.dart
 * @Just do what I think it is right
 */

import 'package:flutter/material.dart';
import 'package:color_dart/color_dart.dart';

import 'package:tacomall_flutter/pages/index/home/index.dart';
import 'package:tacomall_flutter/pages/index/category/index.dart';
import 'package:tacomall_flutter/pages/index/cart/index.dart';
import 'package:tacomall_flutter/pages/index/mine/index.dart';
import 'package:tacomall_flutter/common/icon.dart';

class Index extends StatefulWidget {
  final String routeName;
  final Object arguments;

  static Home _home = Home();
  static Category _category = Category();
  static Cart _cart = Cart();
  static Mine _mine = Mine();

  final Map<int, Map> pages = {
    0: _createPage(_home, appbar: _home.getAppBar(), routeName: '/'),
    1: _createPage(_category,
        appbar: _category.getAppBar(), routeName: '/category'),
    2: _createPage(_cart, appbar: _cart.getAppBar(), routeName: '/cart'),
    3: _createPage(_mine, appbar: _mine.getAppBar(), routeName: '/mine'),
  };

  static Map _createPage(Widget page, {AppBar appbar, String routeName}) {
    return {"widget": page, "appbar": appbar, "routeName": routeName};
  }

  static Index _singleton;

  Index.singleton({this.routeName, this.arguments});

  factory Index({String routeName, Object arguments}) {
    if (_singleton == null) {
      _singleton = Index.singleton(
        routeName: routeName,
        arguments: arguments,
      );
    }

    return _singleton;
  }

  getPageIndex(routeName) {
    switch (routeName) {
      case '/category':
        return 1;
      case '/cart':
        return 2;
      case '/mine':
        return 3;
      default:
        return 0;
    }
  }

  _NavigationState createState() => _NavigationState();
}

class _NavigationState extends State<Index> {
  static int currentIndex = 0;

  @override
  void initState() {
    super.initState();

    Future.delayed(Duration.zero, () {
      String routeName = ModalRoute.of(context).settings.name;
      setState(() {
        currentIndex = widget.getPageIndex(routeName);
      });
    });
  }

  @override
  Widget build(BuildContext context) {
    Map page = widget.pages[currentIndex];
    return Scaffold(
        appBar: page['appbar'],
        body: Container(color: hex('#fff'), child: page['widget']),
        bottomNavigationBar: Theme(
            data: ThemeData(
                highlightColor: Colors.transparent,
                splashColor: Colors.transparent),
            child: BottomNavigationBar(
              type: BottomNavigationBarType.fixed,
              items: [
                BottomNavigationBarItem(
                  icon: iconIndexHome(),
                  title: Text('首页'),
                ),
                BottomNavigationBarItem(
                  icon: iconIndexCategory(),
                  title: Text('分类'),
                ),
                BottomNavigationBarItem(
                  icon: iconIndexCart(),
                  title: Text('购物车'),
                ),
                BottomNavigationBarItem(
                  icon: iconIndexMine(),
                  title: Text('我的'),
                )
              ],
              unselectedFontSize: 10,
              selectedFontSize: 10,
              selectedItemColor: rgba(43, 76, 126, 1),
              currentIndex: currentIndex,
              onTap: (index) {
                setState(() {
                  currentIndex = index;
                });
              },
            )));
  }
}
