/*
 * @Author: 码上talk|RC
 * @Date: 2020-06-18 11:40:26
 * @LastEditTime: 2020-08-25 11:06:50
 * @LastEditors: 码上talk|RC
 * @Description: 
 * @FilePath: /tacomall-flutter/lib/pages/index/home/index.dart
 * @Just do what I think it is right
 */
import 'package:color_dart/color_dart.dart';
import 'package:flutter/material.dart';

import 'package:tacomall_flutter/common/global.dart';
import 'package:tacomall_flutter/ui/widget/swipper/index.dart';

class Home extends StatefulWidget {
  static _HomeState _homeState;

  Home() {
    _homeState = _HomeState();
  }

  getAppBar() => _homeState.createAppBar();

  _HomeState createState() => _HomeState();
}

class _HomeState extends State<Home> {
  List _type;

  initState() {
    super.initState();
    _type = [
      {'name': '推荐'},
      {'name': '服装'}
    ];
  }

  AppBar createAppBar() {
    return null;
  }

  Widget _buildType() {
    List<Widget> lt = [];
    for (var i = 0; i < _type.length; i++) {
      lt.add(Container(
        child: Center(
          child: Text(_type[i]['name']),
        ),
        margin: EdgeInsets.fromLTRB(
            i == 0 ? 0 : G.screenUtil().setWidth(20), 0, 0, 0),
      ));
    }
    return Row(
      children: lt,
    );
  }

  @override
  Widget build(BuildContext context) {
    return Column(
      children: <Widget>[
        Container(
          margin: EdgeInsets.fromLTRB(0, G.screenUtil().setHeight(40), 0, 0),
          child: Column(
            children: <Widget>[
              Container(
                height: G.screenUtil().setHeight(88),
                padding: EdgeInsets.fromLTRB(G.screenUtil().setWidth(27), 0,
                    G.screenUtil().setWidth(27), 0),
                child: Row(
                  children: <Widget>[
                    Image.asset(
                      'lib/res/image/navi-title-v4@138x40.png',
                      width: G.screenUtil().setWidth(160),
                      height: G.screenUtil().setHeight(58),
                    ),
                    Expanded(
                      child: Container(
                        height: G.screenUtil().setHeight(58),
                        margin: EdgeInsets.fromLTRB(
                            G.screenUtil().setWidth(20), 0, 0, 0),
                        decoration: BoxDecoration(
                            color: rgba(244, 244, 244, 1),
                            borderRadius: BorderRadius.circular(
                                G.screenUtil().setWidth(5))),
                        child: Center(
                          child: Text('搜索好物，共有1354商品'),
                        ),
                      ),
                    )
                  ],
                ),
              ),
              Container(
                height: G.screenUtil().setHeight(60),
                padding: EdgeInsets.fromLTRB(G.screenUtil().setWidth(27), 0,
                    G.screenUtil().setWidth(27), 0),
                child: _buildType(),
              )
            ],
          ),
        ),
        Expanded(
            child: SingleChildScrollView(
                child: Column(children: <Widget>[
          Container(
            child: Stack(
              children: <Widget>[
                Positioned(
                    child: TCSwiper([
                  'lib/res/image/index-swipper-1@1440x710.jpg',
                  'lib/res/image/index-swipper-2@1440x710.jpg'
                ], height: G.screenUtil().setHeight(370)))
              ],
            ),
          ),
        ])))
      ],
    );
  }
}
