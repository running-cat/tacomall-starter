/*
 * @Author: 码上talk|RC
 * @Date: 2020-08-25 09:04:26
 * @LastEditTime: 2020-08-25 09:07:34
 * @LastEditors: 码上talk|RC
 * @Description: 
 * @FilePath: /tacomall-flutter/lib/request/domain/user.dart
 * @微信:  13680065830
 * @邮箱:  3189482282@qq.com
 * @oops: Just do what I think it is right
 */
import 'package:dio/dio.dart';
import 'package:flutter/material.dart';

class ReqUser {
  final Dio _dio;
  ReqUser(this._dio);

  Future<Response> register(
      {@required String account, @required String passwd}) {
    return _dio.post('/user/register',
        queryParameters: {"account": account, "passwd": passwd});
  }
}
