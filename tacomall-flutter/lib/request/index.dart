/*
 * @Author: 码上talk|RC
 * @Date: 2020-08-25 09:04:20
 * @LastEditTime: 2020-08-25 09:12:26
 * @LastEditors: 码上talk|RC
 * @Description: 
 * @FilePath: /tacomall-flutter/lib/request/index.dart
 * @微信:  13680065830
 * @邮箱:  3189482282@qq.com
 * @oops: Just do what I think it is right
 */
import 'package:dio/dio.dart';
import 'package:tacomall_flutter/libs/dio.dart';
import 'package:tacomall_flutter/request/domain/user.dart';

class Request {
  Dio _dio;

  Request() {
    _dio = dioInit();
  }

  ReqUser get user => ReqUser(_dio);
}
