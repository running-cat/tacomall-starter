/*
 * @Author: 码上talk|RC
 * @Date: 2020-06-18 08:46:57
 * @LastEditTime: 2020-08-27 09:24:39
 * @LastEditors: 码上talk|RC
 * @Description: 
 * @FilePath: /tacomall-flutter/lib/main.dart
 * @微信:  13680065830
 * @邮箱:  3189482282@qq.com
 * @oops: Just do what I think it is right
 */
import 'package:flutter/material.dart';
import 'package:tacomall_flutter/common/global.dart';
import 'package:tacomall_flutter/router/index.dart';

final Router router = Router();

void main() {
  G.init(() {
    runApp(MyApp());
  });
}

class MyApp extends StatefulWidget {
  MyApp({Key key}) : super(key: key);

  @override
  _MyAppState createState() => _MyAppState();
}

class _MyAppState extends State<MyApp> {
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      navigatorKey: G.navigatorKey,
      initialRoute: '/tracker',
      onGenerateRoute: router.getRoutes,
    );
  }
}
