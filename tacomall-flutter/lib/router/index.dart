/*
 * @Author: 码上talk|RC
 * @Date: 2020-06-18 11:59:19
 * @LastEditTime: 2020-08-27 09:48:20
 * @LastEditors: 码上talk|RC
 * @Description: 
 * @FilePath: /tacomall-flutter/lib/router/index.dart
 * @Just do what I think it is right
 */
import 'package:flutter/material.dart';

import 'package:tacomall_flutter/pages/index/index.dart';
import 'package:tacomall_flutter/pages/login/index.dart';

class Router {
  static final _routes = {
    '/': (BuildContext context, {Object args}) => Index(),
    '/login': (BuildContext context, {Object args}) => Login()
  };

  static Router _singleton;

  Router._internal();

  factory Router() {
    if (_singleton == null) {
      _singleton = Router._internal();
    }

    return _singleton;
  }

  Route getRoutes(RouteSettings settings) {
    String routeName = settings.name;
    final Function builder = Router._routes[routeName];

    if (builder == null) {
      return null;
    } else {
      return MaterialPageRoute(
          settings: settings,
          builder: (BuildContext context) =>
              builder(context, args: settings.arguments));
    }
  }
}
