/*
 * @Author: 码上talk|RC
 * @Date: 2020-08-25 08:56:06
 * @LastEditTime: 2020-08-25 09:18:32
 * @LastEditors: 码上talk|RC
 * @Description: 
 * @FilePath: /tacomall-flutter/lib/libs/dio.dart
 * @微信:  13680065830
 * @邮箱:  3189482282@qq.com
 * @oops: Just do what I think it is right
 */
import 'package:dio/dio.dart';
import 'package:tacomall_flutter/config/app.dart';

Dio dioInit() {
  BaseOptions _baseOptions = BaseOptions(
    baseUrl: configAppReqeustBaseURL,
  );

  Dio dio = Dio(_baseOptions);

  dio.interceptors
      .add(InterceptorsWrapper(onRequest: (RequestOptions options) async {
    return options;
  }, onResponse: (Response response) async {
    return response;
  }, onError: (DioError e) async {
    return e;
  }));

  return dio;
}
