/*
 * @Author: 码上talk|RC
 * @Date: 2020-08-25 11:49:23
 * @LastEditTime: 2020-08-25 11:52:22
 * @LastEditors: 码上talk|RC
 * @Description: 
 * @FilePath: /tacomall-flutter/lib/ui/widget/button/index.dart
 * @微信:  13680065830
 * @邮箱:  3189482282@qq.com
 * @oops: Just do what I think it is right
 */
import 'package:flutter/material.dart';

import 'customize_button.dart';

class TCButton {
  static Widget normal(
      {double width,
      double height = 44,
      String type = 'default',
      Color color,
      Color bgColor,
      Color borderColor,
      bool plain = false,
      VoidCallback onPressed,
      Widget child,
      EdgeInsetsGeometry padding,
      BorderRadius borderRadius}) {
    return CustomizeButton.normal(
            width: width,
            height: height,
            type: type,
            color: color,
            bgColor: bgColor,
            borderColor: borderColor,
            plain: plain,
            onPressed: onPressed,
            child: child,
            padding: padding,
            borderRadius: borderRadius)
        .widget;
  }

  static Widget icon(
      {double width,
      double height = 44,
      String type = 'default',
      Color color,
      Color bgColor,
      Color borderColor,
      bool plain = false,
      VoidCallback onPressed,
      Widget textChild,
      Widget icon,
      EdgeInsetsGeometry padding,
      BorderRadius borderRadius}) {
    return CustomizeButton.icon(
            width: width,
            height: height,
            type: type,
            color: color,
            bgColor: bgColor,
            borderColor: borderColor,
            plain: plain,
            onPressed: onPressed,
            textChild: textChild,
            icon: icon,
            padding: padding,
            borderRadius: borderRadius)
        .widget;
  }

  static Widget loading({
    double width,
    double height = 44,
    String type = 'default',
    Color color,
    Color bgColor,
    Color borderColor,
    bool plain = false,
    VoidCallback onPressed,
    EdgeInsetsGeometry padding,
    BorderRadius borderRadius,
    Widget loadingChild,
  }) {
    return CustomizeButton.loading(
      width: width,
      height: height,
      type: type,
      color: color,
      bgColor: bgColor,
      borderColor: borderColor,
      plain: plain,
      onPressed: onPressed,
      padding: padding,
      borderRadius: borderRadius,
      loadingChild: loadingChild,
    ).widget;
  }
}
