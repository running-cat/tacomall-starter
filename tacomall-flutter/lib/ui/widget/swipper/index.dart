/*
 * @Author: 码上talk|RC
 * @Date: 2020-06-18 16:14:34
 * @LastEditTime: 2020-06-30 10:14:21
 * @LastEditors: 码上talk|RC
 * @Description: 
 * @FilePath: /tacomall-flutter/lib/components/swipper/index.dart
 * @Just do what I think it is right
 */
import 'package:flutter/material.dart';
import 'package:flutter_swiper/flutter_swiper.dart';

class TCSwiper extends StatelessWidget {
  final List<String> images;
  final int index;
  final double height;

  TCSwiper(this.images, {this.index, this.height = 288});

  @override
  Widget build(BuildContext context) {
    return Container(
      height: height,
      child: Swiper(
          index: index,
          itemBuilder: (BuildContext context, int index) {
            return Image.asset(images[index], fit: BoxFit.cover);
          },
          itemCount: images.length,
          pagination: SwiperPagination(
              builder: DotSwiperPaginationBuilder(size: 8, activeSize: 8)),
          autoplay: true,
          duration: 500,
          autoplayDelay: 5000),
    );
  }
}
