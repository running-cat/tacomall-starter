/*
 * @Author: 码上talk|RC
 * @Date: 2020-06-30 10:23:38
 * @LastEditTime: 2020-08-25 08:51:59
 * @LastEditors: 码上talk|RC
 * @Description: 
 * @FilePath: /tacomall-flutter/lib/utils/screen_util.dart
 * @Just do what I think it is right
 */
import 'package:flutter_screenutil/flutter_screenutil.dart';

class TCScreenUtil {
  TCScreenUtil(context) {
    ScreenUtil.instance = ScreenUtil(width: 750, height: 1334)..init(context);
  }

  double setWidth(double width) => ScreenUtil.getInstance().setWidth(width);

  double setHeight(double height) => ScreenUtil.getInstance().setHeight(height);
}
