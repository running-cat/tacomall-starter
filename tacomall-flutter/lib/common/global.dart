/*
 * @Author: 码上talk|RC
 * @Date: 2020-08-24 11:22:13
 * @LastEditTime: 2020-08-25 11:31:10
 * @LastEditors: 码上talk|RC
 * @Description: 
 * @FilePath: /tacomall-flutter/lib/common/global.dart
 * @微信:  13680065830
 * @邮箱:  3189482282@qq.com
 * @oops: Just do what I think it is right
 */

import 'package:flutter/material.dart';
import 'package:fluttertoast/fluttertoast.dart';

import 'package:tacomall_flutter/request/index.dart';
import 'package:tacomall_flutter/utils/screen_util.dart';

class G {
  static final List toobarRouteNameList = ['/', '/category', '/cart', '/mine'];

  static final GlobalKey<NavigatorState> navigatorKey = GlobalKey();

  static NavigatorState getCurrentState() => navigatorKey.currentState;
  static BuildContext getCurrentContext() => navigatorKey.currentContext;

  static TCScreenUtil screenUtil() => TCScreenUtil(getCurrentContext());

  static Future toast(String text) => Fluttertoast.showToast(
      msg: text,
      toastLength: Toast.LENGTH_SHORT,
      gravity: ToastGravity.CENTER,
      timeInSecForIos: 1,
      backgroundColor: Colors.red,
      textColor: Colors.white,
      fontSize: 16.0);

  static void routerPush(String routeName, {Object arguments}) {
    if (toobarRouteNameList.indexOf(routeName) > -1) {
      getCurrentState().pushReplacementNamed(
        routeName,
        arguments: arguments,
      );
    } else {
      getCurrentState().pushNamed(routeName, arguments: arguments);
    }
  }

  static void routerPop() => getCurrentState().pop();

  static final Request req = Request();

  static Future init(VoidCallback callback) async {
    WidgetsFlutterBinding.ensureInitialized();
    callback();
  }
}
