/*
 * @Author: 码上talk|RC
 * @Date: 2020-06-18 14:17:23
 * @LastEditTime: 2020-08-24 11:20:41
 * @LastEditors: 码上talk|RC
 * @Description: 
 * @FilePath: /tacomall-flutter/lib/common/app_bar/index.dart
 * @Just do what I think it is right
 */
import 'package:flutter/material.dart';
import 'package:color_dart/color_dart.dart';

import 'package:tacomall_flutter/common/icon.dart';

AppBar customAppbar(
    {BuildContext context,
    String title = '',
    bool borderBottom = true,
    List actions}) {
  Border borderBottomStyle({Color color, bool show = true}) {
    return Border(
        bottom: BorderSide(
            color: (color == null || !show)
                ? (show ? rgba(242, 242, 242, 1) : Colors.transparent)
                : color,
            width: 1));
  }

  return AppBar(
    centerTitle: true,
    title: Text(
      title,
      style: TextStyle(
          color: rgba(56, 56, 56, 1),
          fontSize: 18,
          fontWeight: FontWeight.bold),
    ),
    backgroundColor: hex('#fff'),
    elevation: 0,
    leading: context == null
        ? null
        : InkWell(
            child: iconGBack(color: rgba(44, 44, 44, 1), size: 16),
            onTap: () => Navigator.pop(context),
          ),
    bottom: PreferredSize(
      child: Container(
        decoration:
            BoxDecoration(border: borderBottomStyle(show: borderBottom)),
      ),
      preferredSize: Size.fromHeight(0),
    ),
    actions: actions,
  );
}
