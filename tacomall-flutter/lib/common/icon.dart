/*
 * @Author: 码上talk|RC
 * @Date: 2020-06-18 12:03:22
 * @LastEditTime: 2020-06-18 14:26:07
 * @LastEditors: 码上talk|RC
 * @Description: 
 * @FilePath: /tacomall-flutter/lib/components/icon/index.dart
 * @Just do what I think it is right
 */
import 'package:flutter/material.dart';

Icon iconGBack({double size = 18.0, Color color}) => Icon(
      IconData(0xe64e, fontFamily: 'iconfont'),
      size: size,
      color: color,
    );

Icon iconIndexHome({double size = 18.0, Color color}) => Icon(
      IconData(0xe6af, fontFamily: 'iconfont'),
      size: size,
      color: color,
    );

Icon iconIndexCategory({double size = 18.0, Color color}) => Icon(
      IconData(0xe63b, fontFamily: 'iconfont'),
      size: size,
      color: color,
    );

Icon iconIndexCart({double size = 18.0, Color color}) => Icon(
      IconData(0xe69b, fontFamily: 'iconfont'),
      size: size,
      color: color,
    );

Icon iconIndexMine({double size = 18.0, Color color}) => Icon(
      IconData(0xe60b, fontFamily: 'iconfont'),
      size: size,
      color: color,
    );
