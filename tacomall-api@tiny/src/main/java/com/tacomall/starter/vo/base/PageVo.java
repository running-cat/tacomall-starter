/***
 * @Author: 码上talk|RC/3189482282@qq.com
 * @Date: 2022-03-20 21:47:15
 * @LastEditTime: 2022-03-20 21:48:22
 * @LastEditors: 码上talk|RC
 * @Description: 
 * @FilePath: /tacomall-api@tiny/src/main/java/com/tacomall/starter/vo/base/PageVo.java
 */
package com.tacomall.starter.vo.base;

import java.sql.Timestamp;
import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.util.HashMap;

public class PageVo extends HashMap<String, Object> {
    private static final long serialVersionUID = 1L;

    @Override
    public Object put(String key, Object value) {
        if (value != null) {
            String type = value.getClass().getSimpleName();
            if (type.equals("Timestamp")) {
                Timestamp times = (Timestamp) value;
                LocalDateTime time = times.toLocalDateTime();
                String format = time.format(DateTimeFormatter.ofPattern("yyyy-MM-dd HH:mm:ss"));
                return super.put(key, format);
            } else {
                return super.put(key, value);
            }
        }
        return super.put(key, value);
    }
}