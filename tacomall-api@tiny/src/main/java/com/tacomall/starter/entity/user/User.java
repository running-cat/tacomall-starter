/***
 * @Author: 码上talk|RC/3189482282@qq.com
 * @Date: 2022-03-20 21:44:30
 * @LastEditTime: 2022-03-20 22:06:34
 * @LastEditors: 码上talk|RC
 * @Description: 
 * @FilePath: /tacomall-api@tiny/src/main/java/com/tacomall/starter/entity/user/User.java
 */
package com.tacomall.starter.entity.user;

import java.time.LocalDateTime;

import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.FieldFill;
import com.baomidou.mybatisplus.annotation.TableField;

import lombok.Data;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.NoArgsConstructor;

@Data
@Builder
@NoArgsConstructor
@AllArgsConstructor
@TableName(autoResultMap = true)
public class User {
    @TableId(value = "id", type = IdType.AUTO)
    private Integer id;

    private String passwd;

    private String mobile;

    private Integer status;

    @TableField(fill = FieldFill.INSERT)
    private Integer isDelete;

    @TableField(fill = FieldFill.INSERT)
    private LocalDateTime createTime;

    @TableField(fill = FieldFill.INSERT_UPDATE)
    private LocalDateTime updateTime;

    private LocalDateTime deleteTime;

}