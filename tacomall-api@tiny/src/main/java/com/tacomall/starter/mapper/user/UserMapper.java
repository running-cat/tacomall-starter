/***
 * @Author: 码上talk|RC/3189482282@qq.com
 * @Date: 2022-03-20 21:45:44
 * @LastEditTime: 2022-03-20 21:46:53
 * @LastEditors: 码上talk|RC
 * @Description: 
 * @FilePath: /tacomall-api@tiny/src/main/java/com/tacomall/starter/mapper/user/UserMapper.java
 */
package com.tacomall.starter.mapper.user;

import org.springframework.stereotype.Repository;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import org.apache.ibatis.annotations.Param;
import com.baomidou.mybatisplus.core.toolkit.Constants;
import com.baomidou.mybatisplus.core.conditions.Wrapper;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;

import com.tacomall.starter.entity.user.User;
import com.tacomall.starter.vo.base.PageVo;

@Repository
public interface UserMapper extends BaseMapper<User> {
    User queryInfo(@Param(Constants.WRAPPER) Wrapper<User> wrapper);

    IPage<PageVo> queryPage(@Param("page") Page<?> page, @Param(Constants.WRAPPER) Wrapper<User> wrapper);
}
