/***
 * @Author: 码上talk|RC/3189482282@qq.com
 * @Date: 2022-03-20 21:55:40
 * @LastEditTime: 2022-03-20 21:55:52
 * @LastEditors: 码上talk|RC
 * @Description: 
 * @FilePath: /tacomall-api@tiny/src/main/java/com/tacomall/starter/util/PasswordUtil.java
 */
package com.tacomall.starter.util;

import org.apache.commons.codec.digest.DigestUtils;

public class PasswordUtil {

    public static String encode(String passwprd) {
        return DigestUtils.sha256Hex(passwprd);
    }
}
