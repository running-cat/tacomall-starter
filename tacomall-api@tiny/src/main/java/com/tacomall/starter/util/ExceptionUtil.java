/***
 * @Author: 码上talk|RC/3189482282@qq.com
 * @Date: 2022-03-20 21:26:26
 * @LastEditTime: 2022-03-20 21:41:43
 * @LastEditors: 码上talk|RC
 * @Description: 
 * @FilePath: /tacomall-api@tiny/src/main/java/com/tacomall/starter/util/ExceptionUtil.java
 */
package com.tacomall.starter.util;

import com.tacomall.starter.interceptor.exception.*;

public class ExceptionUtil {
    public static void throwBizException(String message) throws BizException {
        throw new BizException(message);
    }

    public static void throwClientException(String message) throws ClientException {
        throw new ClientException(message);
    }

    public static void throwServerException(String message) throws ServerException {
        throw new ServerException(message);
    }

    public static void throwSqlException(String message) throws SqlException {
        throw new SqlException(message);
    }

    public static void throwUnauthorizedException(String message) throws UnauthorizedException {
        throw new UnauthorizedException(message);
    }
}
