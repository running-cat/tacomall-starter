package com.tacomall.starter.interceptor.exception;

public class ServerException extends RuntimeException {
    public ServerException(String message) {
        super(message);
    }
}
