/***
 * @Author: 码上talk|RC/3189482282@qq.com
 * @Date: 2022-03-20 21:29:36
 * @LastEditTime: 2022-03-20 21:38:08
 * @LastEditors: 码上talk|RC
 * @Description: 
 * @FilePath: /tacomall-api@tiny/src/main/java/com/tacomall/starter/interceptor/ExceptionInterceptor.java
 */
package com.tacomall.starter.interceptor;

import javax.servlet.http.HttpServletRequest;

import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.bind.annotation.RestControllerAdvice;

import com.tacomall.starter.ret.ResponseRet;
import com.tacomall.starter.enumeration.BizEnum;
import com.tacomall.starter.enumeration.ExceptionEnum;
import com.tacomall.starter.interceptor.exception.*;

@RestControllerAdvice
public class ExceptionInterceptor {

    @ExceptionHandler(value = BizException.class)
    @ResponseBody
    @ResponseStatus(HttpStatus.OK)
    public ResponseRet<String> bizErrorHandler(HttpServletRequest req, BizException e) throws Exception {
        ResponseRet<String> bizError = new ResponseRet<>();
        bizError.setStatus(false);
        bizError.setCode(BizEnum.ERROR.getCode());
        bizError.setMessage(e.getMessage());
        return bizError;
    }

    @ExceptionHandler(value = SqlException.class)
    @ResponseBody
    @ResponseStatus(HttpStatus.OK)
    public ResponseRet<String> sqlErrorHandler(HttpServletRequest req, SqlException e) throws Exception {
        ResponseRet<String> sqlError = new ResponseRet<>();
        sqlError.setStatus(false);
        sqlError.setCode(ExceptionEnum.SERVER_ERROR.getCode());
        sqlError.setMessage(e.getMessage());
        return sqlError;
    }

    @ExceptionHandler(value = UnauthorizedException.class)
    @ResponseBody
    @ResponseStatus(HttpStatus.OK)
    public ResponseRet<String> unauthorizedHandler(HttpServletRequest req, UnauthorizedException e) throws Exception {
        ResponseRet<String> unauthorizedError = new ResponseRet<>();
        unauthorizedError.setStatus(false);
        unauthorizedError.setCode(ExceptionEnum.AUTHORIZED_ERROR.getCode());
        unauthorizedError.setMessage(e.getMessage());
        return unauthorizedError;
    }
}