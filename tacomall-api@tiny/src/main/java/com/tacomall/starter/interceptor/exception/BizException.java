/***
 * @Author: 码上talk|RC
 * @Date: 2021-09-18 14:01:09
 * @LastEditTime: 2022-03-20 21:40:24
 * @LastEditors: 码上talk|RC
 * @Description: 
 * @FilePath: /tacomall-api@tiny/src/main/java/com/tacomall/starter/interceptor/exception/BizException.java
 * @微信:  13680065830
 * @邮箱:  3189482282@qq.com
 * @oops: Just do what I think it is right
 */
package com.tacomall.starter.interceptor.exception;

public class BizException extends RuntimeException {
    public BizException(String message) {
        super(message);
    }
}
