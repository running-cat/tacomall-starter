package com.tacomall.starter.interceptor.exception;

public class SqlException extends RuntimeException {
    public SqlException(String message) {
        super(message);
    }
}
