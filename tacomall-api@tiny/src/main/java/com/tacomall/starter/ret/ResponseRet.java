/***
 * @Author: 码上talk|RC
 * @Date: 2021-04-30 14:13:26
 * @LastEditTime: 2022-03-20 21:41:22
 * @LastEditors: 码上talk|RC
 * @Description: 
 * @FilePath: /tacomall-api@tiny/src/main/java/com/tacomall/starter/ret/ResponseRet.java
 * @微信:  13680065830
 * @邮箱:  3189482282@qq.com
 * @oops: Just do what I think it is right
 */
package com.tacomall.starter.ret;

import com.tacomall.starter.enumeration.BizEnum;

import lombok.Data;

@Data
public class ResponseRet<T> {

    private Boolean status = false;

    private Integer code = BizEnum.ERROR.getCode();

    private String message = BizEnum.ERROR.getMessage();

    private T data;

    public void ok() {
        this.status = true;
        this.code = BizEnum.OK.getCode();
        this.message = BizEnum.OK.getMessage();
    }

}