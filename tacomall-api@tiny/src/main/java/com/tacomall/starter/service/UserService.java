/***
 * @Author: 码上talk|RC/3189482282@qq.com
 * @Date: 2022-03-20 21:21:00
 * @LastEditTime: 2022-03-20 21:49:39
 * @LastEditors: 码上talk|RC
 * @Description: 
 * @FilePath: /tacomall-api@tiny/src/main/java/com/tacomall/starter/service/UserService.java
 */
package com.tacomall.starter.service;

import com.alibaba.fastjson.JSONObject;
import com.baomidou.mybatisplus.extension.service.IService;

import com.tacomall.starter.entity.user.User;
import com.tacomall.starter.ret.ResponseRet;

public interface UserService extends IService<User> {

    ResponseRet<User> register(String mobile, String passwd);

    ResponseRet<String> loginByMobile(String mobile, String passwd);

    ResponseRet<User> info(JSONObject json);

}