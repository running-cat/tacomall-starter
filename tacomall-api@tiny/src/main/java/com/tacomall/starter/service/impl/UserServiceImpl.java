/***
 * @Author: 码上talk|RC/3189482282@qq.com
 * @Date: 2022-03-20 21:21:15
 * @LastEditTime: 2022-03-20 21:57:44
 * @LastEditors: 码上talk|RC
 * @Description: 
 * @FilePath: /tacomall-api@tiny/src/main/java/com/tacomall/starter/service/impl/UserServiceImpl.java
 */
package com.tacomall.starter.service.impl;

import java.util.HashMap;
import cn.hutool.core.util.ObjectUtil;
import org.springframework.core.env.Environment;
import com.alibaba.fastjson.JSONObject;
import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.datasource.DataSourceTransactionManager;
import org.springframework.stereotype.Service;
import org.springframework.transaction.TransactionDefinition;

import com.tacomall.starter.service.UserService;
import com.tacomall.starter.entity.user.User;
import com.tacomall.starter.ret.ResponseRet;
import com.tacomall.starter.mapper.user.UserMapper;
import com.tacomall.starter.util.ExceptionUtil;
import com.tacomall.starter.util.JwtUtil;
import com.tacomall.starter.util.PasswordUtil;
import com.tacomall.starter.util.RequestUtil;

@Service
public class UserServiceImpl extends ServiceImpl<UserMapper, User> implements UserService {

    private final Logger logger = LoggerFactory.getLogger(this.getClass());

    @Autowired
    Environment environment;

    @Autowired
    TransactionDefinition transactionDefinition;

    @Autowired
    DataSourceTransactionManager dataSourceTransactionManager;

    @Override
    public ResponseRet<User> register(String mobile, String passwd) {
        ResponseRet<User> responseRet = new ResponseRet<>();
        User userRegister = new User();
        userRegister.setMobile(mobile);
        userRegister.setPasswd(PasswordUtil.encode(passwd));
        baseMapper.insert(userRegister);
        responseRet.setData(userRegister);
        responseRet.ok();
        return responseRet;
    }

    @Override
    public ResponseRet<String> loginByMobile(String mobile, String passwd) {
        ResponseRet<String> responseRet = new ResponseRet<>();
        User user = baseMapper.selectOne(new QueryWrapper<User>().lambda().eq(User::getMobile, mobile)
                .eq(User::getPasswd, PasswordUtil.encode(passwd)));
        if (ObjectUtil.isNull(user)) {
            responseRet.setMessage("账号或密码错误");
            return responseRet;
        }
        try {
            JwtUtil ju = new JwtUtil();
            ju.setISSUER(environment.getProperty("spring.application.name"));
            responseRet.setData(ju.create(new HashMap<String, String>() {
                {
                    put("id", user.getId().toString());
                }
            }));
            responseRet.ok();
        } catch (Exception e) {
            ExceptionUtil.throwServerException("token生成失败");
        }
        return responseRet;
    }

    @Override
    public ResponseRet<User> info(JSONObject json) {
        ResponseRet<User> responseRet = new ResponseRet<>();
        responseRet.setData(baseMapper
                .selectOne(new QueryWrapper<User>().lambda().eq(User::getId,
                        RequestUtil.getLoginUser().getString("id"))));
        responseRet.ok();
        return responseRet;
    }

}
