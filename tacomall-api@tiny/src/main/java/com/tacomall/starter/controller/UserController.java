/***
 * @Author: 码上talk|RC/3189482282@qq.com
 * @Date: 2022-03-18 21:14:06
 * @LastEditTime: 2022-03-20 21:40:35
 * @LastEditors: 码上talk|RC
 * @Description: 
 * @FilePath: /tacomall-api@tiny/src/main/java/com/tacomall/starter/controller/UserController.java
 */
package com.tacomall.starter.controller;

import com.alibaba.fastjson.JSONObject;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import com.tacomall.starter.annotation.LoginUser;
import com.tacomall.starter.service.UserService;
import com.tacomall.starter.entity.user.User;
import com.tacomall.starter.ret.ResponseRet;

@RestController
@RequestMapping(value = "/user/")
public class UserController {

    @Autowired
    private UserService userService;

    @PostMapping("register")
    public ResponseRet<User> register(@RequestParam(value = "mobile") String mobile,
            @RequestParam(value = "passwd") String passwd) {
        return userService.register(mobile, passwd);
    }

    @PostMapping("loginByMobile")
    public ResponseRet<String> loginByMobile(@RequestParam(value = "mobile") String mobile,
            @RequestParam(value = "passwd") String passwd) {
        return userService.loginByMobile(mobile, passwd);
    }

    @LoginUser
    @PostMapping("info")
    public ResponseRet<User> info(@RequestBody JSONObject json) {
        return userService.info(json);
    }

}
