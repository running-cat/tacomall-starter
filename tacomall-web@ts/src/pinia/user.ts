import { defineStore } from 'pinia'

export default defineStore({
    id: 'userPinia',
    state: () => ({
        info: {}
    }),
    getters: {
        isLogin: (state) => !!state.info
    },
    actions: {
        login(info: any) {
            this.$patch({
                info: { ...info }
            })
        }
    }
})
