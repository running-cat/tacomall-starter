export declare interface Props {
    [key: string]: any;
}

export declare interface Column {
    key: string;
    label: string;
    render?: Function;
}

export declare interface TableMeta {
    column: Array<Column>,
    row: Array<any>
}