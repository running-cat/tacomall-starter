import { h, render } from 'vue'
import dialog from './dialog.vue'

let wrapperMinZIndex: number = 99

const getEleWrapper = (): Element => {
	const eleWrapper = document.createElement('div')
	eleWrapper.setAttribute('class', 'dialog_wrapper')
	eleWrapper.style.zIndex = String(wrapperMinZIndex)
	wrapperMinZIndex++
	return eleWrapper
}


export default (child: any, props: Object) => {
	return new Promise((resolve, reject) => {
		const eleWrapper = getEleWrapper();
		document.body.appendChild(eleWrapper)
		render(h(dialog, {
			_visible:  true,
			_leaveHook: () => {
				document.querySelector('body')?.removeChild(eleWrapper)
			},
			...props
		}, h(child, {  })), eleWrapper)
	})
}