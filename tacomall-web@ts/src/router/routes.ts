export default [
    { path: '/', redirect: '/index' },
    {
        path: "/index",
        name: "index",
        component: () => import('@/views/index.vue')
    },
]