import { defineConfig } from 'vite'
import vue from '@vitejs/plugin-vue'
import { resolve } from 'path'
import vueJsx from '@vitejs/plugin-vue-jsx'

// https://vitejs.dev/config/
export default defineConfig({
  plugins: [vue(), vueJsx()],
  define: {
    'process.env': {},
  },
  resolve: {
    alias: {
      '@': resolve(__dirname, './src'),
    },
  },
  css: {
    postcss: {
      plugins: [
        require('postcss-px-to-viewport')({
          unitToConvert: 'px',
          viewportWidth: 1920,
          viewportHeight: 1080,
          unitPrecision: 3,
          propList: [
            '*'
          ],
          viewportUnit: 'vw',
          fontViewportUnit: 'vw',
          selectorBlackList: [],
          minPixelValue: 1,
          mediaQuery: false,
          replace: true
        })
      ]
    },
    preprocessorOptions: {
      less: {
        charset: false,
        additionalData:`@import "./src/assets/less/var.less";
        @import "./src/assets/less/global.less";
        @import "./src/assets/less/mixins.less";
        @import "./src/assets/font/iconfont.less";`,
      },
    }
  }
})
